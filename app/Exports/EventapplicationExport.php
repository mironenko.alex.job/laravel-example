<?php

    namespace App\Exports;

    use App\Models\Application\Eventapplication;
    use App\Models\Application\Stapplication;
    use Illuminate\Contracts\View\View;
    use Maatwebsite\Excel\Concerns\FromView;

    class EventapplicationExport implements FromView
    {
        public function view(): View
        {
            $applications = Eventapplication::with('event');
            if (request()->all()) {
                foreach (request()->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $applications
                                    ->where('active', $value);
                                break;
                            default:
                                $applications = $applications
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.eventapplications.export', [
                'data' => $applications->get(),
            ]);
        }
    }