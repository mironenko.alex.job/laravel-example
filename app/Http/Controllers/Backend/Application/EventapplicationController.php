<?php

    namespace App\Http\Controllers\Backend\Application;

    use App\Exports\EventapplicationExport;
    use App\Models\Application\Eventapplication;
    use App\Models\Event\Event;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Maatwebsite\Excel\Facades\Excel;

    class EventapplicationController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $applications = Eventapplication::with('event');
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $applications
                                    ->where('active', $value);
                                break;
                            default:
                                $applications = $applications
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }

            return view('backend.eventapplications.index', [
                'applications' => $applications->latest()->paginate(10),
                'events'       => Event::get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {

        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $application         = Eventapplication::find($id);
            $application->active = true;
            $application->update();
            return redirect()->back()->with('success', 'Заявка обновлена успешно');
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }

        /**
         * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
         */
        public function export()
        {
            return Excel::download(new EventapplicationExport(), 'events-application' . date('Y-m-d') . '.xlsx');
        }

        /**
         * @return \Illuminate\Http\RedirectResponse
         */
        public function updateAll()
        {
            Eventapplication::where('active', 0)->update(['active' => 1]);
            return redirect('backend/eventapplications/')
                ->with('success', 'Записи успешно обновлены');
        }
    }
