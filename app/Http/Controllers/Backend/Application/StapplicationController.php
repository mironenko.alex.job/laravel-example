<?php

    namespace App\Http\Controllers\Backend\Application;

    use App\Exports\InvoicesExport;
    use App\Exports\StapplicationExport;
    use App\Http\Requests\Application\StapplicationRequest;
    use App\Models\Application\Stapplication;
    use App\Models\Course\Course;
    use App\Models\Locations\City;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Maatwebsite\Excel\Facades\Excel;

    class StapplicationController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $applications = Stapplication::with('course', 'city');
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $applications
                                    ->where('active', $value);
                                break;
                            default:
                                $applications = $applications
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.stapplications.index', [
                'applications' => $applications->latest()->paginate(10),
                'courses'      => Course::orderBy('title')->get(),
                'cities'       => City::orderBy('title')->get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.stapplications.create', [
                'courses' => Course::orderBy('title')->get(),
                'cities'  => City::orderBy('title')->get(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(StapplicationRequest $request)
        {
            Stapplication::create([
                'username'    => $request->username,
                'email'       => $request->email,
                'phone'       => $request->phone,
                'course_id'   => $request->course_id,
                'city_id'     => $request->city_id,
                'description' => $request->description,
                'active'      => isset($request->active),
            ]);
            return redirect('backend/stapplications/')
                ->with('success', 'Запись создана успешно');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.stapplications.edit', [
                'application' => Stapplication::find($id),
                'courses'     => Course::orderBy('title')->get(),
                'cities'      => City::orderBy('title')->get(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(StapplicationRequest $request, $id)
        {
            $application              = Stapplication::find($id);
            $application->username    = $request->username;
            $application->email       = $request->email;
            $application->phone       = $request->phone;
            $application->course_id   = $request->course_id;
            $application->city_id     = $request->city_id;
            $application->description = $request->description;
            $application->active      = isset($request->active);
            $application->update();
            return redirect('backend/stapplications/')
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $application = Stapplication::find($id);
            $application->delete();
            return redirect('backend/stapplications/')
                ->with('success', 'Запись успешно удалена');
        }

        /**
         * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
         */
        public function export(){
            return Excel::download(new StapplicationExport(), 'students-application' . date('Y-m-d') . '.xlsx');
        }

        /**
         * @return \Illuminate\Http\RedirectResponse
         */
        public function updateAll()
        {
            Stapplication::where('active', 0)->update(['active' => 1]);
            return redirect('backend/stapplications/')
                ->with('success', 'Записи успешно обновлены');
        }
    }
