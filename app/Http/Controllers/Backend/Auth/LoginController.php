<?php

    namespace App\Http\Controllers\Auth;

    use App\Http\Controllers\Controller;
    use App\Models\Setting\WhiteIp;
    use Illuminate\Foundation\Auth\AuthenticatesUsers;
    use Illuminate\Http\Request;


    class LoginController extends Controller
    {
        /*
        |--------------------------------------------------------------------------
        | Login Controller
        |--------------------------------------------------------------------------
        |
        | This controller handles authenticating users for the application and
        | redirecting them to your home screen. The controller uses a trait
        | to conveniently provide its functionality to your applications.
        |
        */

        use AuthenticatesUsers;

        /**
         * Where to redirect users after login.
         *
         * @var string
         */
        protected $redirectTo = '/';

        /**
         * Create a new controller instance.
         *
         */
        public function __construct()
        {
            $this->middleware('guest')->except('logout');
        }

        public function loginForm()
        {

            if(auth()->check()) {

                return redirect()->back();
            }

            return view('backend.auth.login');
        }

        public function login (Request $request)
        {
            // white ip
            auth()->attempt(['email' => $request->email, 'password' => $request->pwd]);
            if (auth()->check()) {
                return response()->json(['auth' => true]);
            }
            return response()->json(['auth' => false], 401);
        }

        public function logout ()
        {
            auth()->logout();
            return redirect('/');
        }
    }
