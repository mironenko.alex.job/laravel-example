<?php

    namespace App\Http\Controllers\Backend\Blog;

    use App\Http\Requests\Blog\AuthorRequest;
    use App\Models\Blog\Author;
    use App\Repositories\Blog\AuthorRepository;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class AuthorController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('backend.blog.authors.index', [
                'authors' => Author::latest()->paginate(50),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.blog.authors.create', [
                'locales' => config('translatable.locales'),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(AuthorRequest $request)
        {
            $author = Author::create($request->all());
            if ($request->thumbnail) {
                $author->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('authors')
                    ->put($author->id . '/' . $author->thumbnail, File::get($request->thumbnail));
                $author->update();
            }
            return redirect(route('blog.authors.edit', $author->id))->with('success', 'Запись успешно создана');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.blog.authors.edit', [
                'author'  => Author::findOrFail($id),
                'locales' => config('translatable.locales'),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(AuthorRequest $request, $id)
        {
            $author = Author::findOrFail($id);
            if ($request->thumbnail) {
                File::cleanDirectory(storage_path('public/images/authors/' .
                $author->id . '/'));
                $author->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('authors')
                    ->put($author->id . '/' . $author->thumbnail, File::get($request->thumbnail));
            }
            $author->update($request->all());
            return redirect(route('blog.authors.edit', $author->id))->with('success', 'Запись успешно обновлена');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $author = Author::findOrFail($id);
            File::deleteDirectory(storage_path('public/images/authors/' . $author->id .
        '/'));
            $author->delete();
            return redirect(route('blog.authors.index'))->with('success', 'Запись успешно удалена');
        }
    }
