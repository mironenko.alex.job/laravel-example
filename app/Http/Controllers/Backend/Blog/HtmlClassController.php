<?php

    namespace App\Http\Controllers\Backend\Blog;

    use App\Models\Blog\Htmlclass;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class HtmlClassController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('backend.blog.html-classes.index', [
                'htmlClasses' => Htmlclass::latest()->paginate(50),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.blog.html-classes.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $htmlClass = Htmlclass::create($request->all());

            return redirect(route('blog.html-classes.edit', $htmlClass->id))->with('success', 'Запись успешно создана');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.blog.html-classes.edit', [
                'htmlClass' => Htmlclass::findOrFail($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $htmlClass = Htmlclass::findOrFail($id);
            $htmlClass->update($request->all());

            return redirect(route('blog.html-classes.edit', $htmlClass->id))->with('success', 'Запись успешно обновлена');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Htmlclass::destroy($id);
            return redirect(route('blog.html-classes.index'))->with('success', 'Запись успешно удалена.');
        }
    }
