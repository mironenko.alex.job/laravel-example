<?php

    namespace App\Http\Controllers\Backend\Blog;

    use App\Http\Requests\Blog\TagRequest;
    use App\Models\Blog\Htmlclass;
    use App\Models\Blog\Tag;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class TagController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('backend.blog.tags.index', [
                'tags' => Tag::with(['htmlClass'])->latest()->paginate(50),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.blog.tags.create', [
                'htmlClasses' => Htmlclass::get()->pluck('html_class', 'id')->toArray(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(TagRequest $request)
        {
            $tag = Tag::create($request->all());

            return redirect(route('blog.tags.edit', $tag->id))->with('success', 'Запись успешно создана');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.blog.tags.edit', [
                'tag' => Tag::findOrFail($id),
                'htmlClasses' => Htmlclass::get()->pluck('html_class', 'id')->toArray(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(TagRequest $request, $id)
        {
            $tag = Tag::findOrFail($id);
            $tag->update($request->all());

            return redirect(route('blog.tags.edit', $tag->id))->with('success', 'Запись успешно обновлена');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Tag::destroy($id);
            return redirect(route('blog.tags.index'))->with('success', 'Запись успешно удалена.');
        }
    }
