<?php

    namespace App\Http\Controllers\Backend\Contact;

    use App\Helpers\Helpers;
    use App\Http\Requests\Contact\CityRequest;
    use App\Models\Locations\City;
    use App\Models\Locations\Country;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CityController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('backend.contacts.cities.index', [
                'data' => City::get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.contacts.cities.create', [
                'html_classes' => explode('|', Helpers::getConfig('links_html_classes')),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(CityRequest $request)
        {
            City::create([
                'country_id'   => Country::first()->id,
                'alias'        => Helpers::checkAlias('App\Models\Locations\City', str_slug($request->title)),
                'title'        => $request->title,
                'address'      => $request->address,
                'phones'       => Helpers::arrayFilter($request->phones),
                'social_links' => Helpers::mutateLinks($request->social_links),
                'email'        => $request->email,
                'lat'          => $request->lat,
                'lng'          => $request->lng,
                'active'       => isset($request->active),
            ]);

            return redirect('backend/cities')->with('success', 'Данные сохранены успешно.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.contacts.cities.edit', [
                'data'         => City::find($id),
                'html_classes' => explode('|', Helpers::getConfig('links_html_classes')),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(CityRequest $request, $id)
        {
            $city               = City::find($id);
            $city->lat          = $request->lat;
            $city->lng          = $request->lng;
            $city->address      = $request->address;
            $city->phones       = Helpers::arrayFilter($request->phones);
            $city->social_links = Helpers::mutateLinks($request->social_links);
            $city->email        = $request->email;
            $city->active       = isset($request->active);
            $city->update();

            return redirect('backend/cities')->with('success', 'Данные обновлены успешно.');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
