<?php

    namespace App\Http\Controllers\Backend\Contact;

    use App\Helpers\Helpers;
    use App\Models\Contact\Sociallink;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class SociallinkController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            return view('backend.contacts.sociallinks.index', [
                'html_classes' => explode('|', Helpers::getConfig('links_html_classes')),
                'data'         => Sociallink::get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            if ($request->has('sn')) {
                $social_links = [];
                $sn_ids       = [];
                foreach ($request->sn as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            if ($key == 'id') {
                                array_push($sn_ids, $v);
                            }
                            $social_links[$k][$key] = $v;
                        }
                    }
                }
                if ($sn_ids) {
                    Sociallink::whereNotIn('id', $sn_ids)->delete();
                }

                if ($social_links) {
                    foreach ($social_links as $social_link) {
                        if (array_key_exists('link', $social_link)) {
                            Sociallink::updateOrCreate($social_link);
                        }
                    }
                }
            }
            return redirect('backend/sociallinks')->with('success', 'Данные сохранены успешно.');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
