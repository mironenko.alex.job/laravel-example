<?php

    namespace App\Http\Controllers\Backend\Partner;

    use App\Http\Requests\Partner\PartnerRequest;
    use App\Models\Partner\Partner;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class PartnerController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $partners = Partner::where('id', '>', 0);
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $partners
                                    ->where('active', $value);
                                break;
                            default:
                                $partners = $partners
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.partners.index', [
                'partners' => $partners->orderBy('position')->paginate(50),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.partners.create', [
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(PartnerRequest $request)
        {
            $links   = [];
            $partner = Partner::create([
                'title'       => $request->title,
                'link'        => $request->link,
                'introtext'   => $request->introtext,
                'description' => $request->description,
            ]);
            if ($request->thumbnail) {
                $partner->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('partners')
                    ->put($partner->id . '/' . $partner->thumbnail, File::get($request->thumbnail));
            }
            if ($request->logo) {
                $partner->logo = md5($request->logo->getClientOriginalName()) . '.' . $request->logo->getClientOriginalExtension();
                Storage::disk('partners')
                    ->put($partner->id . '/' . $partner->logo, File::get($request->logo));
            }
            if ($request->links) {
                foreach ($request->links as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            $links[$k][$key] = $v;
                        }
                    }
                }
                if ($links) {
                    $partner->links = json_encode($links, JSON_UNESCAPED_UNICODE);
                }
            }
            $partner->update();
            return redirect('backend/partners/')
                ->with('success', 'Запись создана успешно');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {

            return view('backend.partners.edit', [
                'partner' => Partner::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $links                = [];
            $partner              = Partner::find($id);
            $partner->title       = $request->title;
            $partner->link        = $request->link;
            $partner->introtext   = $request->introtext;
            $partner->description = $request->description;
            if ($request->thumbnail) {
                $partner->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('partners')
                    ->put($partner->id . '/' . $partner->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/partners/' . $partner->id . '/' . $request->thumbnail_old);
                }
            }
            if ($request->logo) {
                $partner->logo = md5($request->logo->getClientOriginalName()) . '.' . $request->logo->getClientOriginalExtension();
                Storage::disk('partners')
                    ->put($partner->id . '/' . $partner->logo, File::get($request->logo));
                if ($request->logo_old) {
                    Storage::delete('public/images/partners/' . $partner->id . '/' . $request->logo_old);
                }
            }
            if ($request->links) {
                foreach ($request->links as $key => $value) {
                    foreach ($value as $k => $v) {
                        if ($v) {
                            $links[$k][$key] = $v;
                        }
                    }
                }
                if ($links) {
                    $partner->links = json_encode($links, JSON_UNESCAPED_UNICODE);
                }
            }
            $partner->update();
            return redirect('backend/partners/')
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $partner = Partner::find($id);
            Storage::deleteDirectory('public/images/partners/' . $partner->id . '/');
            $partner->delete();
            return redirect('backend/partners/')
                ->with('success', 'Запись успешно удалена');
        }

        /**
         * Sorting
         *
         * @param Request $request
         */
        public function sort(Request $request)
        {
            foreach ($request->items as $key => $value) {
                $item           = Partner::find($value);
                $item->position = $key;
                $item->update();
            }
        }
    }
