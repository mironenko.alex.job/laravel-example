<?php

    namespace App\Http\Controllers\Backend\Review;

    use App\Http\Requests\Review\ReviewRequest;
    use App\Models\Course\Course;
    use App\Models\Review\Review;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;

    class ReviewController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $reviews = Review::with('course');
            if ($request->all()) {
                foreach ($request->all() as $key => $value) {
                    if (isset($value))
                        switch ($key) {
                            case 'page':
                                break;
                            case 'active':
                                $reviews
                                    ->where('active', $value);
                                break;
                            default:
                                $reviews = $reviews
                                    ->where($key, 'like', "%" . $value . "%");
                                break;
                        }
                }
            }
            return view('backend.reviews.index', [
                'reviews' => $reviews->latest()->paginate(10),
                'courses' => Course::orderBy('title')->get(),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend.reviews.create', [
                'courses' => Course::orderBy('title')->get(),
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(ReviewRequest $request)
        {
            $review = Review::create([
                'username'     => $request->username,
                'email'        => $request->email,
                'course_id'    => $request->course_id,
                'review'       => $request->review,
                'active'       => isset($request->active),
                'from_student' => isset($request->from_student),
            ]);
            if ($request->thumbnail) {
                $review->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('reviews')
                    ->put($review->id . '/' . $review->thumbnail, File::get($request->thumbnail));
                $review->update();
            }
            return redirect('backend/reviews/')
                ->with('success', 'Запись создана успешно');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend.reviews.edit', [
                'review'  => Review::find($id),
                'courses' => Course::orderBy('title')->get(),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $review               = Review::find($id);
            $review->username     = $request->username;
            $review->email        = $request->email;
            $review->course_id    = $request->course_id;
            $review->review       = $request->review;
            $review->active       = isset($request->active);
            $review->from_student = isset($request->from_student);
            if ($request->thumbnail) {
                $review->thumbnail = md5($request->thumbnail->getClientOriginalName()) . '.' . $request->thumbnail->getClientOriginalExtension();
                Storage::disk('reviews')
                    ->put($review->id . '/' . $review->thumbnail, File::get($request->thumbnail));
                if ($request->thumbnail_old) {
                    Storage::delete('public/images/reviews/' . $review->id . '/' . $request->thumbnail_old);
                }
            }
            $review->update();
            return redirect('backend/reviews/')
                ->with('success', 'Запись обновлена успешно');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $review = Review::find($id);
            Storage::deleteDirectory('public/images/reviews/' . $review->id . '/');
            $review->delete();
            return redirect('backend/reviews/')
                ->with('success', 'Запись успешно удалена');
        }
    }
