<?php

namespace App\Http\Controllers\Frontend\Course;

use App\Helpers\Helpers;
use App\Http\Controllers\Frontend\Redirect\RedirectController;
use App\Http\ViewComposers\ModalFormFeedbackComposer;
use App\Models\Course\Action;
use App\Models\Course\Course;
use App\Models\Locations\City;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;

class CourseController extends Controller
{
    public function index(Request $request, $alias)
    {
        $course = Course::with([
            'courses_teachers',
            'programs',
            'images',
            'reviews',
        ])
            ->where([
                'alias' => $alias,
                'active' => 1,
            ])->first();

        $cities = $course->courses_cities;
        $c_city = Cookie::get('c_city');
        $courses = Course::where([
            'active' => true,
        ])->whereHas('courses_cities', function ($query) {
            $query->where('city_id', Helpers::getCityid());
        })->orderBy('position')->get();

        if ($course) {
            if (Cookie::get('c_city')) {
                foreach ($cities as $city) {
                    if ($city->id == Cookie::get('c_city')) {

                        return view('frontend.course.index', [
                            'courseData' => $course,
                            'courses' => $courses,
                            'cities' => City::orderBy('title')->get(),
                            'action' => Action::where('active', true)->where('end_date', '>', Carbon::now())->where('id', Helpers::getCityidaction())->first(),
                            'c_city' => $c_city,
                        ]);
                    }
                }

                if($course->related_course){
                    return redirect($course->related_course);
                }else{
                    return redirect(route('home'));
                }
            } else {
                $c_city = 0;
                return view('frontend.course.index', [
                    'courseData' => $course,
                    'courses' => $courses,
                    'cities' => City::orderBy('title')->get(),
                    'action' => Action::where('active', true)->where('end_date', '>', Carbon::now())->where('id', Helpers::getCityidaction())->first(),
                    'c_city' => $c_city,
                ]);
            }
        } else {
            return app(RedirectController::class)->index($request->path());
        }
    }
}
