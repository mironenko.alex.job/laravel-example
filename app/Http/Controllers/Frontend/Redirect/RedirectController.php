<?php

namespace App\Http\Controllers\Frontend\Redirect;

use App\Models\Redirect\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RedirectController extends Controller
{
    public function index($url)
    {
        $redirect = Redirect::where('source', '/' . trim($url, '/') . '/')->first();
        if ($redirect) {
            return redirect($redirect->target, 301);
        } else {
            abort(404);
        }
    }
}
