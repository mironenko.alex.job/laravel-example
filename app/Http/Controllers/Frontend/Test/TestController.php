<?php

    namespace App\Http\Controllers\Frontend\Test;

    use App\Http\Helpers\TestHelpers;
    use App\Models\Course\Course;
    use App\Models\Test\Test;
    use App\Models\Test\TestAnswer;
    use App\Models\Test\TestContent;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class TestController extends Controller
    {

        public function index(Request $request)
        {
            if (!$request->id) {
                session()->forget('answers');
                return view('frontend.tests.index', [
                    'test' => Test::with('answers')->orderBy('id')->first(),
                    'data' => TestContent::first(),
                ]);
            } else {
//                if ($request->has('answer_id')) {
                $checkResult = TestHelpers::check($request->test_id, $request->answer_id ?? 0);
                if (isset($checkResult['courseId'])) {
                    session()->put('courseId', $checkResult['courseId']);
                    session()->forget('answers');
                    return redirect()->route('tests.result');
                }
//                }
                $testId   = isset($checkResult['testId']) ? $checkResult['testId'] : $request->id;
                $test     = Test::with('answers')->findOrFail($testId);
                $prevTest = Test::where('id', '<', $test->id)->orderBy('id', 'DESC')->first();
                $nextTest = Test::where('id', '>', $test->id)->orderBy('id')->first();
            }
            return view('frontend.tests.test', [
                'test'     => $test,
                'prevTest' => $prevTest ?? null,
                'nextTest' => $nextTest ?? Test::orderBy('id')->first(),
                'data'     => TestContent::first(),
            ]);

        }

        /**
         * @param Course $course
         *
         * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
         */
        public function result()
        {
            $course = Course::FindOrFail(session()->get('courseId'));
            session()->forget('courseId');
            return view('frontend.tests.result', [
                'course' => $course,
                'data'   => TestContent::first(),
            ]);
        }

    }
