<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class ArticleRequest extends FormRequest
{
    const RULE_SELECT = 'required|array|min:1';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alias'            => 'required|string|regex:/^[-a-z0-9]+$/|unique:articles,alias,'
                . $this->route('article'),
            'tags_articles'    => self::RULE_SELECT,
            'authors_articles' => self::RULE_SELECT,
            App::getLocale() . '.title' => 'required|min:3',
            App::getLocale() . '.content' => 'required|min:3',
        ];
    }
}
