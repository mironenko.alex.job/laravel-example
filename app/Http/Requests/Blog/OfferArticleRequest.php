<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class OfferArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'  => 'required|min:3',
            'email'     => 'required|email',
            'title'     => 'required|min:3',
            'content'   => 'required|min:3',
            'thumbnail' => 'nullable|max:20000|image',

        ];
    }

    public function attributes()
    {
        return [
            'username'  => __('attributes.username'),
            'email'     => __('attributes.email'),
            'title'     => __('attributes.title'),
            'content'   => __('attributes.content'),
            'thumbnail' => __('attributes.thumbnail'),
        ];
    }
}
