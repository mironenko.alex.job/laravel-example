<?php

    namespace App\Http\Requests\Promocode;

    use Illuminate\Foundation\Http\FormRequest;

    class PromocodeRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'code'        => 'required|min:3',
                'valid_until' => 'required|date|date_format:d.m.Y',
                'discount'    => 'numeric|between:0,99999999',
            ];
        }
    }
