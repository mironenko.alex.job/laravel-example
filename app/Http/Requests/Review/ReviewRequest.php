<?php

    namespace App\Http\Requests\Review;

    use Illuminate\Foundation\Http\FormRequest;

    class ReviewRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'username'  => 'required|min:3',
                'email'     => 'required|email',
                'course_id' => 'required|numeric',
                'review'    => 'required|min:3',
                'thumbnail' => 'file|image|mimes:jpeg,jpg,png|max:2000',
            ];
        }

        /**
         * @return array
         */
        public function attributes()
        {
            return [
                'username'  => 'Автор',
                'email'     => 'Email',
                'course_id' => 'Курс',
                'review'    => 'Текст отзыва',
                'thumbnail' => 'Изображение',
            ];
        }
    }
