<?php

namespace App\Http\Requests\Seo;

use Illuminate\Foundation\Http\FormRequest;

class RedirectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'source' => 'required|min:3',
            'target' => 'required|min:3',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'source' => 'Ключ',
            'target' => 'Цель',
        ];
    }
}
