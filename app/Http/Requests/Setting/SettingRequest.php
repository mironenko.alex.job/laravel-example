<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $key = $this->get('id') != '' ? 'required|alpha_dash|max:32|unique:settings,key,'.$this->get('key').',key' : 'required|unique:settings|alpha_dash|max:32';
        return [
            'key'   => $key,
            'value' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'unique'     => 'Ключ должен быть уникальным',
            'alpha_dash' => 'Только цифры, буквы, символ тире и подчёркивания',
            'max'        => 'Ключ может быть не длиннее :max символов',
        ];
    }
}
