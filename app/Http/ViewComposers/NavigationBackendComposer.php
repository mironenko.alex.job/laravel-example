<?php

    namespace App\Http\ViewComposers;


    use App\Helpers\Helpers;
    use App\Models\Application\Callbackapplication;
    use App\Models\Application\Eventapplication;
    use App\Models\Application\Partnerapplication;
    use App\Models\Application\Stapplication;
    use App\Models\Application\Teachapplication;
    use App\Models\Contact\Phone;
    use App\Models\Contact\Sociallink;
    use App\Models\Course\Course;
    use App\Models\Hub\Hub_message;
    use App\Models\Locations\City;
    use App\Models\Project_life\Projectlife_callbacks;
    use App\Models\Review\Review;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\View\View;

    /**
     * Created by PhpStorm.
     * User: artj
     * Date: 18.01.18
     * Time: 11:10
     */
    class NavigationBackendComposer
    {

        /**
         * Bind data to the view.
         *
         * @param  View $view
         *
         * @return void
         */
        public function compose(View $view)
        {
            $newStudentsApplications = Stapplication::where('active', 0)->count();
            $newTeachersApplications = Teachapplication::where('active', 0)->count();
            $newPartnersApplications = Partnerapplication::where('viewed', 0)->count();
            $newReviewsApplications  = Review::where('active', 0)->count();
            $newCallbackApplications = Callbackapplication::where('viewed', 0)->count();
            $newEventApplications    = Eventapplication::where('active', 0)->count();
            $amounts                 = [
                'newStudentsApplications' => $newStudentsApplications > 0 ? $newStudentsApplications : '',
                'newTeachersApplications' => $newTeachersApplications > 0 ? $newTeachersApplications : '',
                'newPartnersApplications' => $newPartnersApplications > 0 ? $newPartnersApplications : '',
                'newReviewsApplications'  => $newReviewsApplications > 0 ? $newReviewsApplications : '',
                'newCallbackApplications' => $newCallbackApplications > 0 ? $newCallbackApplications : '',
                'newEventApplications'    => $newEventApplications > 0 ? $newEventApplications : '',
            ];
            $amounts['total_amount'] = array_sum($amounts);
            $hub_count = Hub_message::where('viewed', null)->count();
            $amounts['total_hub_amount'] =  $hub_count > 0 ? $hub_count: '';
            $project_count = Projectlife_callbacks::where('viewed', null)->count();
            $amounts['total_project_amount'] =  $project_count > 0 ? $project_count : '';

            $amounts['total_amount'] = $amounts['total_amount'] > 0 ? $amounts['total_amount'] : '';
            $view->with($amounts);
        }

    }