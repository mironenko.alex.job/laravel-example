<?php

namespace App\Models\Blog;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use Translatable;

    public $translatedAttributes
        = [
            'seo_title',
            'seo_keywords',
            'seo_description',
            'seo_robots',
            'seo_canonical',
            'seo_content',
        ];
}
