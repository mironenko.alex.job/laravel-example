<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class BlogTranslation extends Model
{
    protected $fillable
        = [
            'description',
            'seo_title',
            'seo_keywords',
            'seo_description',
            'seo_robots',
            'seo_canonical',
            'seo_content',
        ];
}
