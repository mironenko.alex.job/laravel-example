<?php

    namespace App\Models\Contact;

    use Illuminate\Database\Eloquent\Model;

    class Phone extends Model
    {
        protected $guarded = ['id'];

        /**
         * @return mixed
         */
        public function getPhoneLinkAttribute(){
            return str_replace(['-','(', ')', ' '], '', $this->phone);
        }
    }
