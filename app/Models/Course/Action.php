<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $guarded = ['id'];
}
