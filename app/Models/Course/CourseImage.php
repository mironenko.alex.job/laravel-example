<?php

    namespace App\Models\Course;

    use Illuminate\Database\Eloquent\Model;

    class CourseImage extends Model
    {
        /**
         * @return mixed
         */
        public function course()
        {
            return $this->BelongsTo(Course::class)->withTrashed();
        }
    }
