<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseProgram extends Model
{
    protected $guarded = ['id'];

    /**
     * @return mixed
     */
    public function course()
    {
        return $this->BelongsTo(Course::class);
    }
}
