<?php

namespace App\Models\Event;

use Illuminate\Database\Eloquent\Model;

class EventImage extends Model
{
    /**
     * @return mixed
     */
    public function event()
    {
        return $this->BelongsTo(Event::class);
    }
}
