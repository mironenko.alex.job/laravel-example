<?php

    namespace App\Models\Hub;

    use Illuminate\Database\Eloquent\Model;

    class Hub extends Model
    {
        //
        protected $table = 'hub';

        protected $fillable
            = [
                'title',
                'section_introduction_title',
                'section_introduction_description',
                'section_class_rooms_title',
                'section_class_rooms_description',
                'section_get_info_title',
                'section_locations_location_information',
                'section_about_us_content',
                'phone',
                'mail',
                'address',
                'seo_title',
                'seo_keywords',
                'seo_robots',
                'seo_canonical',
                'seo_description',
            ];

    }
