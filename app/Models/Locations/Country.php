<?php

namespace App\Models\Locations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    protected $fillable
        = [
            'title',
            'alias',
        ];

    /**
     * @return mixed
     */
    public function cities()
    {
        return $this->hasMany(City::class)->withTrashed();
    }
}
