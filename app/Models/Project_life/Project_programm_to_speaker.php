<?php

namespace App\Models\Project_life;

use Illuminate\Database\Eloquent\Model;

class Project_programm_to_speaker extends Model
{
    protected $table = 'speker_to_project';
    protected static $tablename = 'speker_to_project';

    static function tablename()
    {
        return self::$tablename;
    }

}
