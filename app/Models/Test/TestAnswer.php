<?php

    namespace App\Models\Test;

    use App\Models\Course\Course;
    use Illuminate\Database\Eloquent\Model;

    class TestAnswer extends Model
    {
        protected $fillable
            = [
                'test_id',
                'key',
                'answer',
                'position',
            ];

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function test()
        {
            return $this->BelongsTo(Test::class);
        }

        /**
         * @return mixed
         */
        public function courses_answers()
        {
            return $this->belongsToMany(Course::class)->withTrashed();
        }
    }
