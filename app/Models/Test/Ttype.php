<?php

namespace App\Models\Test;

use App\Models\Course\Course;
use Illuminate\Database\Eloquent\Model;

class Ttype extends Model
{
    protected $fillable = [
        'name'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function courses_ttypes()
    {
        return $this->belongsToMany(Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tests_ttypes()
    {
        return $this->belongsToMany(Test::class);
    }
}
