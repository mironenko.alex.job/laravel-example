<?php

    namespace App\Models\User;

    use Zizaco\Entrust\EntrustRole;

    class Role extends EntrustRole
    {

        public function permissions()
        {
            return $this->belongsToMany(Permission::class);
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function roles_users()
        {
            return $this->belongsToMany(User::class);
        }
    }
