<?php

    namespace App\Models\User;

    use Illuminate\Notifications\Notifiable;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Zizaco\Entrust\Traits\EntrustUserTrait;

    class User extends Authenticatable
    {
        use Notifiable;
        use EntrustUserTrait;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable
            = [
                'name', 'email', 'password',
            ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden
            = [
                'password', 'remember_token',
            ];

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function roles_users()
        {
            return $this->belongsToMany(Role::class);
        }

        /**
         * get Role user
         * @return mixed
         */
        public function scopeRole()
        {
            return $this->roles->first();
        }
    }
