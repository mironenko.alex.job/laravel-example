<?php

namespace App\Providers;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ((config('app.env') === 'production')) {
            URL::forceScheme('https');
        }
        Schema::defaultStringLength(191);
        View::composer('backend.navigations.main', 'App\Http\ViewComposers\NavigationBackendComposer');
        View::composer('frontend.layouts.app', 'App\Http\ViewComposers\LayoutAppComposer');
        View::composer('frontend.layouts.app-blog', 'App\Http\ViewComposers\LayoutAppComposer');
        View::composer('frontend.tests.template-parts', 'App\Http\ViewComposers\LayoutAppComposer');
        View::composer('frontend.layouts.app-event', 'App\Http\ViewComposers\LayoutAppComposer');
        View::composer('frontend.modals.modal-form-feedback', 'App\Http\ViewComposers\ModalFormFeedbackComposer');
        View::composer('frontend.modals.modal-form-teacher', 'App\Http\ViewComposers\ModalFormTeacherComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
