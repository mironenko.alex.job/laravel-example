<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LaravelLocalizationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/config.php' => config_path('laravellocalization.php'),
        ], 'config');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['modules.handler', 'modules'];
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
//			$packageConfigFile = __DIR__.'/../../config/laravellocalization.php';

//			$this->mergeConfigFrom(
//				$packageConfigFile, 'laravellocalization'
//			);

        $this->registerBindings();

        $this->registerCommands();
    }

    /**
     * Registers app bindings and aliases.
     */
    protected function registerBindings()
    {
        $localizations = new LaravelLocalization();

        if (empty($this->locales)) {
            $locales   = [];
            $directory = base_path('resources/lang/');
            foreach (Finder::create()->in($directory)->directories()->depth(0)->sortByName() as $dir) {
                $dirName = basename($dir->getPathname());
                if ($dirName != 'vendor') {
                    $locales[] = $dirName;
                }
            }

            $locales = array_unique($locales);
            sort($locales);
            $localizations->setSupportedLocales(array_diff($locales, config('laravellocalization.supportedLocales')));
        }

        $this->app->singleton(LaravelLocalization::class, function () use ($localizations) {
            return $localizations;
        });


        $this->app->alias(LaravelLocalization::class, 'laravellocalization');
    }

    /**
     * Registers route caching commands.
     */
    protected function registerCommands()
    {
        $this->app->singleton('laravellocalizationroutecache.cache', RouteTranslationsCacheCommand::class);
        $this->app->singleton('laravellocalizationroutecache.clear', RouteTranslationsClearCommand::class);
        $this->app->singleton('laravellocalizationroutecache.list', RouteTranslationsListCommand::class);

        $this->commands([
            'laravellocalizationroutecache.cache',
            'laravellocalizationroutecache.clear',
            'laravellocalizationroutecache.list',
        ]);
    }
}
