<?php

    return [

        /*
        |--------------------------------------------------------------------------
        | Default Filesystem Disk
        |--------------------------------------------------------------------------
        |
        | Here you may specify the default filesystem disk that should be used
        | by the framework. The "local" disk, as well as a variety of cloud
        | based disks are available to your application. Just store away!
        |
        */

        'default' => env('FILESYSTEM_DRIVER', 'local'),

        /*
        |--------------------------------------------------------------------------
        | Default Cloud Filesystem Disk
        |--------------------------------------------------------------------------
        |
        | Many applications store files both locally and in the cloud. For this
        | reason, you may specify a default "cloud" driver here. This driver
        | will be bound as the Cloud disk implementation in the container.
        |
        */

        'cloud' => env('FILESYSTEM_CLOUD', 's3'),

        /*
        |--------------------------------------------------------------------------
        | Filesystem Disks
        |--------------------------------------------------------------------------
        |
        | Here you may configure as many filesystem "disks" as you wish, and you
        | may even configure multiple disks of the same driver. Defaults have
        | been setup for each driver as an example of the required options.
        |
        | Supported Drivers: "local", "ftp", "s3", "rackspace"
        |
        */

        'disks' => [

            'local' => [
                'driver' => 'local',
                'root'   => storage_path('app'),
            ],

            'public' => [
                'driver'     => 'local',
                'root'       => storage_path('app/public'),
                'url'        => env('APP_URL') . '/storage',
                'visibility' => 'public',
            ],

            'action' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/action'),
                'url'    => '/storage/images/action',
            ],

            'articles' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/articles'),
                'url'    => '/storage/images/articles',
            ],

            'authors' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/authors'),
                'url'    => '/storage/images/authors',
            ],

            'creators' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/creators'),
                'url'    => '/storage/images/creators',
            ],

            'teachers' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/teachers'),
                'url'    => '/storage/images/teachers',
            ],

            'students' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/students'),
                'url'    => '/storage/images/students',
            ],

            'courses' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/courses'),
                'url'    => '/storage/images/courses',
            ],

            'types' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/types'),
                'url'    => '/storage/images/types',
            ],

            'opinions' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/opinions'),
                'url'    => '/storage/images/opinions',
            ],

            'partners' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/partners'),
                'url'    => '/storage/images/partners',
            ],

            'homepage' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/homepage'),
                'url'    => '/storage/images/homepage',
            ],

            'reviews' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/reviews'),
                'url'    => '/storage/images/reviews',
            ],

            'rewards' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/rewards'),
                'url'    => '/storage/images/rewards',
            ],

            'events' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/events'),
                'url'    => '/storage/images/events',
            ],

            'hub' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/hub'),
                'url'    => '/storage/images/hub',
            ],

            'project_life' => [
                'driver' => 'local',
                'root'   => storage_path('app/public/images/project_life'),
                'url'    => '/storage/images/project_life',
            ],

            's3' => [
                'driver' => 's3',
                'key'    => env('AWS_KEY'),
                'secret' => env('AWS_SECRET'),
                'region' => env('AWS_REGION'),
                'bucket' => env('AWS_BUCKET'),
            ],

        ],

    ];
