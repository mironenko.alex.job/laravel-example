<?php

    use App\Models\Blog\Tag;
    use Faker\Generator as Faker;

    $factory->define(Tag::class, function (Faker $faker, $attr) {
        $colorName = $faker->colorName . ' ' . $faker->colorName;
        return [
            'htmlclass_id' => $attr['htmlclass_id'],
            'name'          => $colorName,
            'alias'         => str_slug($colorName) . '-' . $faker->randomDigit,
        ];
    });
