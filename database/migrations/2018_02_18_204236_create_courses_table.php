<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCoursesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('courses', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('active')->default(true);
                $table->string('alias');
                $table->float('cost')->nullable();
                $table->integer('discount')->nullable();
                $table->string('thumbnail')->nullable();
                $table->string('logo')->nullable();
                $table->string('video')->nullable();
                $table->string('title');
                $table->string('introtext')->nullable();
                $table->text('description')->nullable();
                $table->string('seo_title')->nullable();
                $table->string('seo_description')->nullable();
                $table->string('seo_keywords')->nullable();
                $table->string('seo_robots')->nullable();
                $table->string('seo_canonical')->nullable();
                $table->integer('position')->default(0);
                $table->integer('amount_places')->default(0);
                $table->integer('free_places')->default(0);
                $table->boolean('employment')->default(true);
                $table->text('programs')->nullable();
                $table->text('begin')->nullable();
                $table->integer('type_id')->unsigned();
                $table->timestamps();
                $table->softDeletes();
                $table->unique('alias');
                $table->foreign('type_id')->references('id')->on('types')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('courses');
        }
    }
