<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStapplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stapplications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email', 64)->nullable();
            $table->string('phone', 24)->nullable();
            $table->integer('course_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->text('description');
            $table->boolean('active')->default(true);
            $table->foreign('course_id')->references('id')->on('courses')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stapplications');
    }
}
