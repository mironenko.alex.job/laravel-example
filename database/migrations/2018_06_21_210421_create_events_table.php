<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateEventsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('events', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('alias');
                $table->text('description')->nullable();
                $table->text('timeline')->nullable();
                $table->string('thumbnail')->nullable();
                $table->string('template');
                $table->boolean('active')->default(true);
                $table->string('begin')->nullable();
                $table->string('cost')->nullable();
                $table->string('seo_title')->nullable();
                $table->string('seo_robots')->nullable();
                $table->string('seo_keywords')->nullable();
                $table->string('seo_canonical')->nullable();
                $table->string('seo_description')->nullable();
                $table->timestamps();
            });

            Schema::create('event_images', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('event_id')->unsigned();
                $table->string('name')->nullable();
                $table->string('title')->nullable();
                $table->string('alt')->nullable();
                $table->boolean('active')->default(true);
                $table->boolean('cover')->nullable()->default(false);
                $table->integer('sort')->default(1);
                $table->foreign('event_id')->references('id')->on('events')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });

            Schema::create('event_teacher', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('event_id')->unsigned();
                $table->integer('teacher_id')->unsigned();
                $table->foreign('event_id')->references('id')->on('events')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('teacher_id')->references('id')->on('teachers')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('event_teacher');
            Schema::dropIfExists('event_images');
            Schema::dropIfExists('events');
        }
    }
