<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class AlterHomepageTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::dropIfExists('homepage');
            Schema::create('homepages', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('about_section_thumbnail')->nullable();
                $table->string('about_section_title')->nullable();
                $table->text('about_section_content')->nullable();
                $table->string('opinion_section_title')->nullable();
                $table->string('opinion_section_thumbnail')->nullable();
                $table->string('rewards_section_title')->nullable();
                $table->string('rewards_section_thumbnail')->nullable();
                $table->string('discount_section_title')->nullable();
                $table->string('discount_section_thumbnail')->nullable();
                $table->string('seo_description')->nullable();
                $table->string('seo_keywords')->nullable();
                $table->string('seo_robots')->nullable();
                $table->string('seo_canonical')->nullable();
                $table->string('seo_title')->nullable();
                $table->string('partners_section_title')->nullable();
                $table->string('schedule_section_title')->nullable();
                $table->string('teachers_section_title')->nullable();
                $table->text('description')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
//            Schema::dropIfExists('homepages');
        }
    }
