<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateEventapplicationsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('eventapplications', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('event_id')->unsigned();
                $table->string('username');
                $table->string('email', 64)->nullable();
                $table->string('phone', 24)->nullable();
                $table->boolean('viewed')->default(false);
                $table->boolean('active')->default(false);
                $table->foreign('event_id')->references('id')->on('events')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('eventapplications');
        }
    }
