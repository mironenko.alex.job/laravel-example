<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCreatorsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('creators', function (Blueprint $table) {
                $table->increments('id');
                $table->string('username');
                $table->string('specialization')->nullable();
                $table->text('description')->nullable();
                $table->string('thumbnail')->nullable();
                $table->text('social_links')->nullable();
                $table->integer('position')->default(0);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('creators');
        }
    }
