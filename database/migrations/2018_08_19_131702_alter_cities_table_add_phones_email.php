<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class AlterCitiesTableAddPhonesEmail extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('cities', function ($table) {
                $table->text('phones')->nullable();
                $table->text('social_links')->nullable();
                $table->string('email')->nullable();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('cities', function (Blueprint $table) {
                $table->dropColumn(['phones', 'email', 'social_links']);
            });
        }
    }
