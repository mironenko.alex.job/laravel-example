<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectlifePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_life_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',128)->nullable();
            $table->string('logo',256)->nullable();
            $table->string('short_text')->nullable();
            $table->text('text')->nullable();
            $table->string('link')->nullable();
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projectlife_partners');
    }
}
