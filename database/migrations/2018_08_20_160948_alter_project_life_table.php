<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectLifeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_life', function (Blueprint $table) {
            $table->string('date')->nullable();
            $table->string('place')->nullable();
            $table->string('link')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_life', function (Blueprint $table) {
            $table->dropColumn(['date', 'place', 'link']);
        });
    }
}
