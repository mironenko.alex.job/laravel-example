<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateTestsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('tests', function (Blueprint $table) {
                $table->increments('id');
                $table->string('key', 4)->nulllable()->index();
                $table->text('question')->nullable();
                $table->integer('position')->default(0);
                $table->timestamps();
            });

            Schema::create('test_answers', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('test_id')->unsigned();
                $table->string('key', 3)->nullable()->index();
                $table->text('answer')->nullable();
                $table->integer('position')->default(0);
                $table->timestamps();

                $table->foreign('test_id')->references('id')->on('tests')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

            Schema::create('course_answer', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id')->unsigned();
                $table->integer('answer_id')->unsigned();
                $table->timestamps();

                $table->unique(['course_id', 'answer_id']);
                $table->foreign('course_id')->references('id')->on('courses')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('answer_id')->references('id')->on('test_answers')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('course_answer');
            Schema::dropIfExists('test_answers');
            Schema::dropIfExists('tests');
        }
    }
