<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class RenameCourseAnswerToCourseTestAnswer extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::rename('course_answer', 'course_test_answer');

            Schema::table('course_test_answer', function (Blueprint $table) {
                $table->renameColumn('answer_id', 'test_answer_id');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::rename('course_test_answer', 'course_answer');
            Schema::table('course_answer', function (Blueprint $table) {
                $table->renameColumn('test_answer_id', 'answer_id');
            });
        }
    }
