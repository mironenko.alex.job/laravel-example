<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoursesTableAddDiscountEndDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function($table) {
            $table->dateTime('discount_end_date')->nullable();
            $table->dropColumn([
                'action_start_date',
                'is_action_enabled',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function(Blueprint $table) {
            $table->dateTime('action_start_date')->nullable();
            $table->boolean('is_action_enabled')->default(false);
            $table->dropColumn('discount_end_date');
        });
    }
}
