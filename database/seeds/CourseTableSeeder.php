<?php

    use App\Models\Course\Course;
    use Illuminate\Database\Seeder;

    class CourseTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker = Faker\Factory::create();
            factory(Course::class, 15)->create();
            $courses = Course::get();
            foreach ($courses as $course) {
                Storage::deleteDirectory('public/images/courses/' . $course->id);
                Storage::makeDirectory('public/images/courses/' . $course->id, 755);
                $course->thumbnail = $faker->image(storage_path('app/public/images/courses/' . $course->id), 400, 300, 'city', false);
                $course->logo      = $faker->image(storage_path('app/public/images/courses/' . $course->id), 400, 300, 'abstract', false);
                $course->update();
            }
        }
    }
