<?php

    use App\Models\Blog\Htmlclass;
    use Illuminate\Database\Seeder;

    class DatabaseSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $this->call(PermissionTableSeeder::class);
            $this->call(RolesTableSeeder::class);
            $this->call(UserTableSeeder::class);
            $this->call(LocationSeeder::class);
            $this->call(TypeSeeder::class);
            $this->call(CourseTableSeeder::class);
            $this->call(TeacherSeeder::class);
            // $this->call(StudentSeeder::class);
            $this->call(PartnerTableSeeder::class);
            $this->call(ReviewTableSeeder::class);
            $this->call(RewardTableSeeder::class);
            $this->call(StapplicationTableSeeder::class);
            $this->call(TeachapplicationTableSeeder::class);
            $this->call(DiscountTableSeeder::class);
            $this->call(SeoscriptsTableSeeder::class);
            $this->call(SettingTableSeeder::class);
            $this->call(CourseCityTableSeeder::class);
            $this->call(CourseUpdateTableSeeder::class);
            $this->call(HomepageTableSeeder::class);
            $this->call(EventTableSeeder::class);
            $this->call(CourseProgramSeeder::class);
            $this->call(TelegramconfigTableSeeder::class);
            $this->call(Hub_roomsTableSeeder::class);
            $this->call(TestTableSeeder::class);
            $this->call(TtypeTableSeeder::class);
            $this->call(HtmlClassesSeeder::class);
            $this->call(TagSeeder::class);
            $this->call(AuthorSeeder::class);
            $this->call(ArticleSeeder::class);
        }
    }
