<?php

    use App\Models\Event\Event;
    use App\Models\Teacher\Teacher;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\Storage;

    class EventTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $faker = Faker\Factory::create();
            factory(Event::class, 15)->create();
            $events = Event::get();
            foreach ($events as $event) {
                Storage::deleteDirectory('public/images/events/' . $event->id);
                Storage::makeDirectory('public/images/events/' . $event->id, 0777);
                $event->thumbnail = $faker->image(storage_path('app/public/images/events/' . $event->id), 400, 300, 'cats', false);
                $event->events_teachers()->sync(Teacher::all()->random(1)->first()->id);
                $event->update();
            }
        }
    }
