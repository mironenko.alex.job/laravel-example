<?php

    use App\Models\Blog\Htmlclass;
    use Illuminate\Database\Seeder;

class HtmlClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $htmlClasses = [
            [
                'html_class' => 'bg--l-blue'
            ],
            [
                'html_class' => 'bg--d-blue'
            ],
            [
                'html_class' => 'bg--or'
            ],
        ];
        foreach ($htmlClasses as $htmlClass){
            Htmlclass::create($htmlClass);
        }
    }
}
