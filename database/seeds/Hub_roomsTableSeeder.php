<?php

use Illuminate\Database\Seeder;

class Hub_roomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

            [
                'room_key' => 'hall',
                'room_name'  => 'Конференц. зал',
                'room_image' => 'alevel-hub/images/rooms/1.png',
                'room_image_location' => 'alevel-hub/images/map/conferent.png',
                'capacity_text' => '100 чел.',
                'room_information' => '       <ul>                                     <li>Проводите конференции и семинары</li>                                     <li>Большая стена с проектором для презентаций</li>                                     <li>wi-fi, конференц-сервис, напитки, печенюшки и пункт жизнеобеспечения — в вашем распоряжении</li>                                     <li>Удобное расположение пространства</li>                                     <li>Аудитория как тренинг зал, зал для семинаров или зал для конференций, выбирать вам!</li>                                     <li>Стоимость: 300 грн/час</li>                                 </ul>',
            ],

            [
                'room_key' => 'greyroom',
                'room_name'  => 'Grey room',
                'room_image' => 'alevel-hub/images/rooms/2.png',
                'room_image_location' => 'alevel-hub/images/map/grey.png',
                'capacity_text' => '30 чел',
                'room_information' => '       <ul>                                     <li>Проводите конференции и семинары</li>                                     <li>Большая стена с проектором для презентаций</li>                                     <li>wi-fi, конференц-сервис, напитки, печенюшки и пункт жизнеобеспечения — в вашем распоряжении</li>                                     <li>Удобное расположение пространства</li>                                     <li>Аудитория как тренинг зал, зал для семинаров или зал для конференций, выбирать вам!</li>                                     <li>Стоимость: 300 грн/час</li>                                 </ul>',
            ],

            [
                'room_key' => 'blueroom',
                'room_name'  => 'Blue room',
                'room_image' => 'alevel-hub/images/rooms/3.png',
                'room_image_location' => 'alevel-hub/images/map/blue.png',
                'capacity_text' => '30 чел',
                'room_information' => '       <ul>                                     <li>Проводите конференции и семинары</li>                                     <li>Большая стена с проектором для презентаций</li>                                     <li>wi-fi, конференц-сервис, напитки, печенюшки и пункт жизнеобеспечения — в вашем распоряжении</li>                                     <li>Удобное расположение пространства</li>                                     <li>Аудитория как тренинг зал, зал для семинаров или зал для конференций, выбирать вам!</li>                                     <li>Стоимость: 300 грн/час</li>                                 </ul>',
            ],

            [
                'room_key' => 'redroom',
                'room_name'  => 'Red room',
                'room_image' => 'alevel-hub/images/rooms/4.png',
                'room_image_location' => 'alevel-hub/images/map/red.png',
                'capacity_text' => '30 чел',
                'room_information' => '       <ul>                                     <li>Проводите конференции и семинары</li>                                     <li>Большая стена с проектором для презентаций</li>                                     <li>wi-fi, конференц-сервис, напитки, печенюшки и пункт жизнеобеспечения — в вашем распоряжении</li>                                     <li>Удобное расположение пространства</li>                                     <li>Аудитория как тренинг зал, зал для семинаров или зал для конференций, выбирать вам!</li>                                     <li>Стоимость: 300 грн/час</li>                                 </ul>',
            ],

            [
                'room_key' => 'allroom',
                'room_name'  => 'allroom',
                'room_image' => 'alevel-hub/images/rooms/4.png',
                'room_image_location' => 'alevel-hub/images/map/all.png',
                'capacity_text' => '30 чел',
                'room_information' => '    инфа о всех   <ul>                                     <li>Проводите конференции и семинары</li>                                     <li>Большая стена с проектором для презентаций</li>                                     <li>wi-fi, конференц-сервис, напитки, печенюшки и пункт жизнеобеспечения — в вашем распоряжении</li>                                     <li>Удобное расположение пространства</li>                                     <li>Аудитория как тренинг зал, зал для семинаров или зал для конференций, выбирать вам!</li>                                     <li>Стоимость: 300 грн/час</li>                                 </ul>',
            ],
        ];

        foreach ($data as $item) {
            \App\Models\Hub\Hub_room::create($item);
        }
    }
}
