<?php

    use App\Models\Locations\City;
    use App\Models\Locations\Country;
    use Illuminate\Database\Seeder;

    class LocationSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $countries = [
                'Украина' => [
                    'Харьков',
                    'Полтава',
                ],
            ];
            foreach ($countries as $key => $values) {
                $country = Country::create([
                    'title' => $key,
                    'alias' => str_slug($key),
                ]);
                foreach ($values as $city) {
                    City::create([
                        'country_id' => $country->id,
                        'title'      => $city,
                        'alias'      => str_slug($city),
                    ]);
                }
            }
        }
    }
