<?php

use Illuminate\Database\Seeder;

class Project_lifeTimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [

       [ 'time' => '10-00'],
       [ 'time' => '11-00'],
       [ 'time' => '12-00'],
       [ 'time' => '13-00'],
       [ 'time' => '14-00'],
       [ 'time' => '15-00'],
       [ 'time' => '16-00'],
       [ 'time' => '17-00'],
       [ 'time' => '18-00'],
       [ 'time' => '19-00'],

        ];

        foreach ($data as $item) {
            \App\Models\Project_life\Time::create($item);
        }

    }
}
