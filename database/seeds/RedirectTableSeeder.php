<?php

use App\Models\Course\Course;
use App\Models\Redirect\Redirect;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class RedirectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = Course::get();
        foreach ($courses as $course) {
            Redirect::create([
                'source' => '/courses/' . $course->alias
            ]);
        }
    }

}
