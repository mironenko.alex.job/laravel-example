<?php

    use App\Models\User\Permission;
    use App\Models\User\Role;
    use Illuminate\Database\Seeder;

    class RolesTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $roles       = [
                [
                    'name'         => 'admin',
                    'display_name' => 'Администратор',
                    'description'  => 'Полный доступ',
                ],
            ];
            $permissions = Permission::all()->pluck('id');
            foreach ($roles as $key => $value) {
                $role = Role::create($value);
                $role->permissions()->sync($permissions);
            }
        }
    }
