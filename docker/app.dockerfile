FROM php:7.1-fpm

RUN apt-get update -y && apt-get install -y libpng-dev

RUN apt-get update && \
    apt-get install -y -qq libjpeg62-turbo-dev \
        libfreetype6-dev \
        libmcrypt-dev \
        git \
        jpegoptim \
        optipng

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd

RUN docker-php-ext-install mbstring zip pdo pdo_mysql