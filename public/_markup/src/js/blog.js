import './common/mainMenu';
import './home/request';
import './home/simplebar';

$(function () {

    $('.js-mask').mask('+38 (000) 000 00 00');

    $('.js-open-modal-request').on('click', function () {

        $('body').addClass('modal-opened')

        PubSub.publishSync('hideScene', {
            param: 'menu-in-row',
            cb: () => {
            $('.navigate').addClass('navigate--row');
        $('#modalFeedback').fadeIn(500).css('display', 'flex');

    }
    });
        return false
    });

    $('.add-info-btn').on('click', function () {

        $('body').addClass('modal-opened')

        PubSub.publishSync('hideScene', {
            param: 'menu-in-row',
            cb: () => {
            $('.navigate').addClass('navigate--row');
        $('#modalBlog').fadeIn(500).css('display', 'flex');

    }
    });
        return false
    });


    $('.modal-close').on('click', function (e) {

        $('#modalFeedback, #modalBlog').hide();
        $('body').removeClass('modal-opened');
        const _$this = $(this);
        const _$cont = _$this.hasClass('modal')
            ? _$this
            : _$this.closest('modal');

        _$cont.find('input').val('');
        _$cont.find('select').each((i, e) => {
            const _$this = $(e);
        const val = _$this.find('option').eq(0).val();

        _$this.val(val).trigger("change");
    });
    });

});