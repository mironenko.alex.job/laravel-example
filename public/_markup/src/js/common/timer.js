const $ = window.$;
$(function () {
  function getTimeRemaining (endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
      'total': t,
      'days': days,
      'hours': hours,
      'minutes': minutes,
      'seconds': seconds
    };
  }

  function initializeClock (id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.js-timer-days');
    var hoursSpan = clock.querySelector('.js-timer-hours');
    var minutesSpan = clock.querySelector('.js-timer-minutes');
    var secondsSpan = clock.querySelector('.js-timer-seconds');

    function updateClock () {
      var t = getTimeRemaining(endtime);

      daysSpan.innerHTML = t.days;
      hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
      minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
      secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

      if (t.total <= 0) {
        clearInterval(timeinterval);
      }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
  }

  function convertDateForIos (date) {
    var arr = date.split(/[- :]/);
    date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
    return date;
  }

  var clock = document.getElementById('clockdiv');
  if (clock) {
    var deadline = convertDateForIos(clock.dataset.date);
    initializeClock('clockdiv', deadline);
  }

  $('#closeSale').on('click', function () {
    $('.sale').hide()
    $('body').removeClass('with-sale')
  })
})
