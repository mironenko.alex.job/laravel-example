import PubSub from 'pubsub-js';
import { TimelineLite } from 'gsap';
import doubleBackground from '../common/doubleBackground'
const $ = window.$;
// Обработчик события goToSlide
PubSub.subscribe('goToSlide', function (msg, data) {

  if (data.from === data.to) return;

  // Ссылка на дом узлы слайдов
  let $currentSlide = $(`[data-slide='${data.from}']`);
  let $newSlide = $(`[data-slide='${data.to}']`);

  $newSlide.show().css('display', 'flex');
  doubleBackground()

  let tl = new TimelineLite({
    onComplete () {
      $currentSlide.hide()
    }
  })
    .set($currentSlide, {zIndex: 20})
    .set($newSlide, {zIndex: 30});

  // display: flex
  $('[data-slide="' + data.to + '"]').addClass('is-active');

  if (data.direction === 1) {
    tl
      .fromTo($currentSlide, data.delay, {left: '0%', top: 0}, {top: 0, left: '-100%'}, 'animation')
      .fromTo($newSlide, data.delay, {left: '100%', top: 0}, {left: '0%', top: 0}, 'animation')
      .call(hidePrevSlide)
  } else if (data.direction === -1) {
    tl
      .fromTo($currentSlide, data.delay, {left: '0%', top: '0%'}, {left: '100%', top: 0}, 'animation')
      .fromTo($newSlide, data.delay, {left: '-100%', top: '0%'}, {left: '0%', top: 0}, 'animation')
      .call(hidePrevSlide)
  }

  function hidePrevSlide () {
    $currentSlide.removeClass('is-active');
    $newSlide.addClass('is-active');
  }
});

PubSub.subscribe('goToSlideInside', function (msg, data) {
  let prev = data.from;
  let next = data.to;
  // Ссылка на дом узлы внутренних слайдов
  let $nextSlideInCourse = $(`[data-slide-course=${next}]`);
  let $prevSlideInCourse = $(`[data-slide-course=${prev}]`);

  if (prev === next) return;

  $nextSlideInCourse.css('display', 'flex');
  // Анимация переключения
  let tl = new TimelineLite({
    onComplete: function () {
      $prevSlideInCourse.css('visibility', 'hidden').hide();
    }
  });

  tl
    .fromTo($prevSlideInCourse, data.delay, {
      y: '0%'
    }, {
      opacity: 0,
      y: data.direction > 0 ? '-100%' : '100%'
    }, 'slideIn')
    .fromTo($nextSlideInCourse, data.delay, {
      y: data.direction > 0 ? '100%' : '-100%'
    }, {
      opacity: 1,
      y: '0%',
      visibility: 'visible'
    }, 'slideIn')
});
