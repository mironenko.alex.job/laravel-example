const $ = window.$;

$(function () {
  $('.course-program-popup__container').html($(`.js-details[data-month=${1}]`).html())

  $('.course-program-duration__item').on('click', function () {
    $('.course-program-duration__item').removeClass('course-program-duration__item--active')
    $(this).addClass('course-program-duration__item--active')
    let month = parseInt(this.dataset.month)
    $('.course-program-description').removeClass('course-program-description--active')
    $(`.course-program-description[data-month=${month}]`).addClass('course-program-description--active')
    $('#showPopupProgram .month').text(month)
    $('.js-change-month .month').text(month)
    $('#showPopupProgram').data('month', month)
    $('.course-program-popup__container').html($(`.js-details[data-month=${month}]`).html())
  })

  $('.course-program-popup__btn').on('click', function () {
    $('.course-program-popup').fadeOut()
  })

  $('#showPopupProgram').on('click', function () {
    let month = $(this).data('month');
    $('.course-program-popup').fadeIn().css('display', 'flex')
  })
})
