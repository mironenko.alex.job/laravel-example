import '../libs/jquery.mask';

const $ = window.$;

$(function () {
  $('.js-mask').mask('+38 (000) 000 00 00');

  $('.js-open-modal-request').on('click', function () {
    $('body.course').addClass('no-scroll');
    $('.navigate').addClass('navigate--row');
    $('#modalFeedback').fadeIn(500).css('display', 'flex');
    $('.scene-bg').addClass('scene-bg--blur');
    $('body').addClass('modal-opened');
    return false
  });

  $('.modal-feedback').on('click', function (e) {
    e.stopPropagation();
  });
  
  $('#modalFeedback, #modalAnswer, #modalTeacher, #modalPartner, .modal-close, .modal').on('click', function (e) {
    $('body.course').removeClass('no-scroll');
    $('.navigate').removeClass('navigate--row');
    $('#modalFeedback, #modalPartner, #modalTeacher, #modalAnswer').hide();
    $('body').removeClass('modal-opened');
    const _$this = $(this);
    const _$cont = _$this.hasClass('modal')
      ? _$this
      : _$this.closest('modal');

    _$cont.find('input').val('');
    _$cont.find('select').each((i, e) => {
      const _$this = $(e);
      const val = _$this.find('option').eq(0).val();

      _$this.val(val).trigger("change");
    });
  });
});
