import {TimelineLite} from 'gsap';
import PubSub from 'pubsub-js';
import 'gsap/src/uncompressed/plugins/ScrollToPlugin';
import config from '../config';

const $ = window.$;
const $html = $('html');

PubSub.subscribe('hideAndShowScene', function (msg, data) {
  let $navigate = $('.navigate');
  $('.modal').hide();
  /*clear form start*/
  const _$cont = $('.modal');
   _$cont.find('input').val('');
  _$cont.find('select').each((i, e) => {
    const _$this = $(e);
  const val = _$this.find('option').eq(0).val();
  // _$this.val(val).trigger("change");
});

  _$cont.find('.js-error-area p').remove();
  _$cont.find('.js-error-area').css('display', 'none');  /*clear form end*/
  $('.trigger').removeClass('trigger--active');
  if (data.to === 0) {
    if ($html.hasClass('ie')) {
      $('.scene-bg').removeClass('ie-bg');
    }

    $navigate.removeClass('navigate--row')

    let tl = new TimelineLite({
      onComplete: () => {
        config.canGo = true;
      }
    })

    tl
      .to($('.layer'), 0, { display: 'none' })
      .to($('.scene-bg'), 0, { filter: 'none' })
      .to(window, 0.5, {
        scrollTo: {
          y: 0
        }
      })
  } else {
    if ($html.hasClass('ie')) {
      $('.scene-bg').addClass('ie-bg');
    }

    let height = $(document).height(),
      numberOfScenes = $('.layer').length + 1,
      depthOfScene = height / numberOfScenes;

    let tl = new TimelineLite({
      onComplete: () => {
        if (data.to) {
          $navigate.addClass('navigate--row');
          // $('body').addClass('modal-active');

          if (data.to === $('.trigger').length) {
            PubSub.publish('map')
          }

          let tl2 = new TimelineLite({
            onComplete: () => {
              $(`.trigger[data-goto]`).eq(data.to - 1).addClass('trigger--active')
              config.canGo = true;
            }
          });

          tl2
            .to(window, 0.5, {
              scrollTo: {
                y: data.to * depthOfScene
              }
            })
            .to($('.scene-bg'), 0, { filter: data.to ? 'blur(10PX) brightness(50%)' : 'none' })
            .fromTo($(`[data-window]`).eq(data.to), 0.5, {
              y: -50
            }, {
              y: 0,
              display: 'flex'
            })
        }
      }
    });

    tl
      .to($('.layer'), 0, {display: 'none'});
  }
});
