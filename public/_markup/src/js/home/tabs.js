import PubSub from 'pubsub-js';

const $ = window.$;

$(function () {
  let checked = '1';
  $('.tabs__item--' + checked).show();

  $('input[name=about]').change(function () {
    $('.js-regard-info').removeClass('active')
    let checked = this.value;

    $('label').css('z-index', 1);

    if (parseInt(checked, 10) === 2) {
      PubSub.publishSync('tab', {
        to: checked,
        img: $('.tabs__item--2').data('backgroung')
      });
      $('#r2 + label').css('z-index', 3);
      $('#r3 + label').css('z-index', 2);
    } else if (parseInt(checked, 10) === 3) {
      PubSub.publishSync('tab', {
        to: checked,
        img: $('.tabs__item--3').data('backgroung')
      });

      $('#r3 + label').css('z-index', 2)
    } else if (parseInt(checked, 10) === 4) {
      PubSub.publishSync('tab', {
        to: checked,
        img: $('.tabs__item--4').data('backgroung')
      });
      $('#r4 + label').css('z-index', 2);
    } else {
      PubSub.publishSync('tab', {
        to: checked,
        img: $('.tabs__item--1').data('backgroung')
      });
      $('#r3 + label').css('z-index', 2);
      $('#r2 + label').css('z-index', 3);
      $('#r1 + label').css('z-index', 4);
    }
  });
});
