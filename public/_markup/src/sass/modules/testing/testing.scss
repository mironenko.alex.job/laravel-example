// .testing
.testing {
  // Надо только на 1 скрине
  // padding: 30px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  height: 100%;

  // .testing__heading
  &__heading {
    margin: 60px 120px 15px 30px;

    text-align: left;
    text-transform: uppercase;

    color: $orange;

    font-family: $OpenSans;
    font-size: 36px;
    line-height: 1.2;
  }

  // .testing__p
  &__p {
    overflow-y: auto;

    max-height: 230px;
    margin: 80px 0;
    padding-right: 5%;

    text-align: left;

    color: $gray;

    font-family: $OpenSans;
    font-size: 22px;
    line-height: 1.4;
  }

  // .testing__btns
  &__btns {
    margin-bottom: 30px;
    margin-left: 30px;

    .btn {
      margin-right: 30px;
    }
  }

  &__steps {
    display: none;

    margin-bottom: 30px;
    margin-left: 30px;

    .btn {
      margin-right: 30px;
    }
  }

  &__finish {
    display: none;

    margin-bottom: 30px;
    margin-left: 30px;

    .btn {
      margin-right: 30px;
    }
  }
}

.animator {
  position: relative 0 0 0 0;

  overflow: hidden;

  width: 95%;
  height: 100%;
  margin-bottom: 15px;
}

[data-slide-testing] {
  position: absolute 0 0 0 0;

  display: flex;
  visibility: hidden;
  overflow-y: auto;
  flex-direction: column;
  justify-content: space-between;

  width: 100%;
  height: 100%;
  padding: 0 120px 0px 30px;

  opacity: 0;
}

.testing-start {
  visibility: visible;

  opacity: 1;
}

.testing-question {
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  padding-top: 30px;
}

//#testing__p::-webkit-scrollbar-track {
//  background-color: $lightGray;
//}
//
//#testing__p::-webkit-scrollbar {
//  width: 6px;
//}
//
//#testing__p::-webkit-scrollbar-thumb {
//  background-color: $orange;
//}

// .progress
.progress {
  display: flex;
  flex-direction: column;

  margin: 0 120px 30px 30px;

  // .progress__percentages
  &__percentages {
    display: flex;

    margin-bottom: 12px;
    margin-left: auto;

    text-align: left;

    color: $gray;

    font-family: $OpenSans;
    font-size: 14px;
    font-weight: bold;
    line-height: 1.2;
  }

  // .progress__line
  &__line {
    display: flex;

    width: 100%;
    height: 4px;

    background-color: $bg;
  }

  // .progress__progress
  &__progress {
    display: flex;

    width: 0%;
    height: 100%;

    transition: all 0.4s ease-in;

    background-color: $orange;
  }
}

// .test
.test {
  // .test__question
  &__body-wrapper {
    padding-top: 5rem;
  }
  &__question {
    margin-bottom: 50px;

    text-align: left;

    color: $blue;

    font-family: $OpenSans;
    font-size: 30px;
    font-weight: bold;
    line-height: 1.2;
  }
}

// .answer
.answer {
  display: flex;
  align-items: center;

  margin-bottom: 30px;

  // .answer__p
  &__p {
    margin-left: 30px;

    color: $gray;

    font-family: $OpenSans;
    font-size: 18px;
  }


  // .answer__radio
  &__radio {
    display: flex;

    // .answer__radio label
    label {
      display: flex;

      cursor: pointer;

      border: 1px solid $blue;
      border-radius: 50%;

      size: 20px;

      // .answer__radio label + input[type=radio]:checked
      & + input[type=radio]:checked {
        background-color: $orange;
      }
    }

    // .answer__radio input[type=radio]
    input[type=radio] {
      display: none;

      // .answer__radio input[type=radio]:checked + label
      &:checked + label {
        &:before {
          display: flex;

          margin: auto;

          content: "";

          border-radius: 50%;
          background-color: $orange;
          box-shadow: inset 0px 0px 0px 2px $white;

          size: 18px;
        }
      }
    }
  }

  // .answer__checkbox
  &__checkbox {
    display: flex;

    // .answer__checkbox label
    label {
      display: flex;
      overflow: hidden;

      cursor: pointer;

      border: 1px solid $blue;

      size: 20px;

      // .answer__checkbox label + input[type=checkbox]:checked
      & + input[type=checkbox]:checked {
        background-color: $orange;
      }
    }

    // .answer__checkbox input[type=checkbox]
    input[type=checkbox] {
      display: none;

      // .answer__checkbox input[type=checkbox]:checked + label
      &:checked + label {
        &:before {
          display: flex;

          margin: auto;

          content: "";

          background-color: $orange;
          box-shadow: inset 0px 0px 0px 1px $white;

          size: 18px;
        }
      }
    }
  }
}

// .subscription
.subscription {
  // .subscription__form
  &__form {
    display: flex;

    margin-bottom: 30px;
  }

  // .subscription__p
  &__p {
    margin: 0;
    margin-bottom: 30px;

    text-align: left;

    color: $gray;

    font-family: $OpenSans;
    font-size: 18px;
    line-height: 1.2;
  }
}

// .result
.result {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  overflow-y: auto;
  font-size: 200%;
  padding: 4rem;

  h1,h2,h3,h4,h5,h6 {
    color: $orange;
    font-size: 24px;
  }
  // .result__heading
  &__heading {
    margin: 0 0 30px;

    text-align: left;

    color: $blue;

    font-family: $OpenSans;
    font-size: 30px;
    font-weight: bold;
    line-height: 1.2;
  }

  // .result__p
  &__p {
    text-align: left;

    color: $gray;

    font-family: $OpenSans;
    font-size: 18px;
    line-height: 1.2;
  }

  // .result__points
  &__points {
    margin-bottom: 30px;
  }

  // .result__point
  &__point {
    text-align: left;

    color: $orange;

    font-family: $OpenSans;
    font-size: 48px;
    font-weight: bold;
    line-height: 1.2;
  }

  // .result__answers
  &__answers {
    margin-left: 25px;

    text-align: left;

    color: $blue;

    font-family: $OpenSans;
    font-size: 30px;
    font-weight: bold;
    line-height: 1.2;
  }
}
