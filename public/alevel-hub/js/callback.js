function handlerModalForm(e) {

    var $form = $(this);

    if ($form.find("#name").val() == '' || $form.find("#phone").val() == '') {
        e.preventDefault();
        return;
    }


    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: "send_message", // CHANAGE IT!
        data: $form.serialize(),
    }).done(function(response) {
        $('#sendRequest').modal('hide');
        $("#success-call").modal();
    }).fail(function() {
        $('#sendRequest').modal('hide');
        $("#bad-call").modal();
    });

    $form.find("#name").val("");
    $form.find("#phone").val("");

    e.preventDefault();
}

$(function() {
    $('#modalRequestForm,#contactForm').submit(handlerModalForm);
});