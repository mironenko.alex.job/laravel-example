$(function() {
  $('#form').submit(function(e) {
    e.preventDefault()
    var th = $(this);
    $.ajax({
      type: "POST",
      url: "mail.php", //Change
      data: th.serialize()
    }).done(function() {
      alert("Thank you!");
      setTimeout(function() {
        // Done Functions
        th.trigger("reset");
      }, 1000);
    });
    return false;
  })

  let responsive = {
    0:{
      items:2,
    },
    768:{
      items:3,
    },
    1000:{
      items:4,
    }
  };

  $('.owl-carousel-1').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    nav : false,
    dots: false,
    autoplay: true,
    autoplaySpeed: 1000,
    autoplayTimeout: 2000,
    startPosition: $('.owl-carousel img').length / 2,
    responsive: responsive
  })

  $('.owl-carousel-2').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    nav : false,
    dots: false,
    autoplay: true,
    autoplaySpeed: 1000,
    autoplayTimeout: 2000,
    responsive: responsive
  })
});


var map;
function initMap() {
  let latitude = 49.9874,
  longitude = 36.2306,
  mapZoom = 17;

  // Адрес до иконки с маркером
  let markerUrl = '../img/markerMap.png';

  let mapOptions = {
    // Координаты
    center: new google.maps.LatLng(latitude, longitude),
    panControl: false,
    // draggable: false,
    zoomControl: false,
    mapTypeControl: false,
    streetViewControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false,
    // styles: style,
    zoom: mapZoom,
    disableDefaultUI: true,
    gestureHandling: 'cooperative'
  };


  // Инициализация карты
  let map = new google.maps.Map(document.getElementById('map'), mapOptions);

  // Маркер на карте
  let marker = new google.maps.Marker({
    position: new google.maps.LatLng(latitude, longitude),
    map: map,
    visible: true,
    // icon: markerUrl
  });
}
