<div class="panel panel-headerless">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">Основная информация</span>
                </a>
            </li>
            <li>
                <a href="#locale" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">Контент</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main">
                <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                    <div class="row form-group">
                        <div class="user-img">
                            <a href="#">
                                <img data-img="thumbnail" src="{{ isset($author) ? $author->thumbnailMini : url('assets/images/user-4.png') }}"
                                     class="img-circle js_image_link"
                                     alt="user-pic"/>
                            </a> Основное изображение
                            <div class="invisible">
                                {{Form::file('thumbnail', ['accept' => 'image/*', 'data-img' => 'thumbnail'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label for="roleName">Email </label>
                        {!! Form::email('email', null, ['class'=>'form-control','placeholder'=>'Email','data-validation'=>'required', 'required']) !!}
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="locale">
                <div class="col-xs-3">
                    <ul class="nav nav-tabs tabs-left">
                        @foreach($locales as $lang)
                            <li class="@if ($loop->first) active @endif"><a data-toggle="tab"
                                                                            href="#v-pills-{{ $lang }}">{{ $lang }}
                                </a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-xs-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach($locales as $lang)
                            <div class="tab-pane  @if ($loop->first) active @endif" id="v-pills-{{ $lang }}">
                                <div class="form-group">
                                    <label for="roleName">Имя {{ $lang }}</label>
                                    {!! Form::textarea('' . $lang . '[first_name]', $author->{'first_name:'.$lang}??'',['class'=>'form-control','placeholder'=>'Имя '.$lang,'data-validation'=>'required', 'required']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="roleName">Фамилия {{ $lang }}</label>
                                    {!! Form::textarea('' . $lang . '[last_name]', $author->{'last_name:'.$lang}??'',['class'=>'form-control','placeholder'=>'Фамилия '.$lang,'data-validation'=>'required', 'required']) !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{ route('blog.authors.index') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            @isset($author)
                <div class="pull-right">
                    <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                    </button>
                </div>
            @endisset
        </div>
    </div>
</div>

@section('scripts')
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Course\CourseRequest')->ignore('') !!}
@endsection