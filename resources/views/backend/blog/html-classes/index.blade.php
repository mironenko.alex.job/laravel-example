@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">HTML-classes</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('blog.html-classes.create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="20%">ID</th>
                            <th width="60%">HTML-class</th>
                            <th width="20%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($htmlClasses as $htmlClass)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $htmlClass->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $htmlClass->html_class !!}</span>
                                </td>
                                <td>
                                    <a href="{!! route('blog.html-classes.edit', $htmlClass->id) !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $htmlClasses->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
