@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Subscribers</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('blog.subscribers.export') }}" class="btn btn-primary"><span class="fa-download"></span> export</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="20%">id</th>
                            <th width="50%">email</th>
                            <th width="30%">создан</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($subscribers as $subscriber)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $subscriber->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $subscriber->email !!}</span>
                                </td>
                                <td>
                                    <span class="email">{!! $subscriber->created_at !!}</span>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $subscribers->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
