@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Сообщение с формы обратной связи</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/callbackapplications') }}">Все сообщения</a>
                </li>
                <li class="active">
                    <strong>Сообщение с формы обратной связи: {{ \Carbon\Carbon::parse($application->created_at)->format('d-m-Y H:i:s') }}</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">

                    <div class="member-form-inputs">
                        <div class="row form-group">
                            <div class="col-sm-3">Имя пользователя</div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{ $application->username }}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">Phone</div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                   {{ $application->phone }}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">Текст заявки</div>
                            <div class="col-sm-9">
                                {!! $application->message !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/callbackapplications') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            </div>
        </div>
    </div>

@endsection

