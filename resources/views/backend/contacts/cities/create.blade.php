@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование города</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/cities') }}">Города</a>
                </li>
                <li class="active">
                    <strong>Редактирование</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('cities.store'), 'method' => 'POST', 'class' => 'validate', "novalidate" => 'novalidate']) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">

                    <div class="member-form-inputs js_parent_div">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('key', 'Название *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('title', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('key', 'Email *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('email', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Email'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('lat', 'Широта', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('lat', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Широта'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('lng', 'Долгота', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::number('lng', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Долгота'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3" for="field-1">Адрес</label>
                            <div class="col-sm-9">
                                {{  Form::textarea('address', null, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('active', 'Активность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('active', 1, 1, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                        <div class="col-sm-12 js_section">
                            <table class="table table-bordered table-striped" id="example-2">
                                <tbody class="middle-align js_parent">
                                <tr class="js_section">
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('cost', 'Номер', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::text('phones[]', null, ['class' => 'form-control js-phone', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Номер'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-2">
                                        <div class="vertical-top">
                                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                                Удалить
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="js_section hidden">
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                {{ Form::label('cost', 'Номер', ['class' => 'control-label']) }}
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    {{  Form::text('phones[]', null, ['class' => 'form-control js-phone', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Номер'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-2">
                                        <div class="vertical-top">
                                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                                Удалить
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="right-aligned">
                                <a href="#" class="btn btn-success pull-right js_add_item"><span class="fa-plus"></span>Добавить</a>
                            </div>
                        </div>
                        <div class="col-sm-12 js_section">
                            <table class="table table-bordered table-striped" id="example-2">
                                <thead>
                                <tr>
                                    <th>HTML - класс</th>
                                    <th>Ссылка</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>

                                <tbody class="middle-align js_parent">

                                <tr class="js_section">
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <select name="social_links[html_class][]">
                                                        @foreach($html_classes as $htmlClass)
                                                            <option value="{{ $htmlClass }}" @if($loop->first) selected="selected" @endif>{{ $htmlClass }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    {{  Form::text('social_links[link][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Ссылка'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-2">
                                        <div class="vertical-top">
                                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                                Удалить
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="js_section hidden">
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <select name="social_links[html_class][]">
                                                        @foreach($html_classes as $htmlClass)
                                                            <option value="{{ $htmlClass }}" @if($loop->first) selected="selected" @endif>{{ $htmlClass }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    {{  Form::text('social_links[link][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Ссылка'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-2">
                                        <div class="vertical-top">
                                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                                Удалить
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="right-aligned">
                                <a href="#" class="btn btn-success pull-right js_add_item"><span class="fa-plus"></span>Добавить</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/cities') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection


