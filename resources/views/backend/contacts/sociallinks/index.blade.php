@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Соцсети</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('sociallinks.store'), 'method' => 'POST', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product']) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#programm" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Программа</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active js_section" id="programm">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped" id="example-2">
                            <thead>
                            <tr>
                                <th>HTML - класс</th>
                                <th>Ссылка</th>
                                <th>Действие</th>
                            </tr>
                            </thead>

                            <tbody class="middle-align js_parent">
                            @forelse ($data as $item)
                                <tr class="js_section">
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <select name="sn[html_class][]">
                                                        @foreach($html_classes as $htmlClass)
                                                            <option value="{{ $htmlClass }}" @if($item->html_class == $htmlClass) selected="selected" @endif>{{ $htmlClass }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    {{  Form::text('sn[link][]', $item->link, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Номер'])}}
                                                </div>
                                            </div>
                                        </div>
                                        {{ Form::hidden('sn[id][]', $item->id) }}
                                    </td>
                                    <td class="col-xs-2">
                                        <div class="vertical-top">
                                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                                Удалить
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr class="js_section">
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <select name="sn[html_class][]">
                                                        @foreach($html_classes as $htmlClass)
                                                            <option value="{{ $htmlClass }}" @if($loop->first) selected="selected" @endif>{{ $htmlClass }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    {{  Form::text('sn[link][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Ссылка'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-2">
                                        <div class="vertical-top">
                                            <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                                Удалить
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforelse
                            <tr class="js_section hidden">
                                <td>
                                    <div class="row form-group">
                                        <div class="col-sm-12">
                                            <div class="input-group">
                                                <select name="sn[html_class][]">
                                                    @foreach($html_classes as $htmlClass)
                                                        <option value="{{ $htmlClass }}" @if($loop->first) selected="selected" @endif>{{ $htmlClass }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row form-group">
                                        <div class="col-sm-12">
                                            <div class="input-group">
                                                {{  Form::text('sn[link][]', null, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Ссылка'])}}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-xs-2">
                                    <div class="vertical-top">
                                        <a href="#" class="btn btn-danger btn-sm btn-icon icon-left js_remove_section">
                                            Удалить
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-aligned">
                        <a href="#" class="btn btn-success pull-right js_add_item"><span class="fa-plus"></span>Добавить</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
