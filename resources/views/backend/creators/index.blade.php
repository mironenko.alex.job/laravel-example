@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Создатели сайта</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ url('backend/creators/create') }}" class="btn btn-success">
                <span class="fa-plus"></span> Добавить
            </a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="20%">Фото</th>
                            <th width="15%">Имя</th>
                            <th width="20%">Специализация</th>
                            <th width="15%">Действие</th>
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ url('backend/creators/sort/') }}">
                        @forelse($creators as $creator)
                            <tr class="js-sortable-item" data-id="{{ $creator->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $creator->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('creators')->url(  $creator->id . '/' . $creator->thumbnail  ), 239, 239) }}" class="img-circle" alt="user-pic"/>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $creator->username !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $creator->specialization !!}</span>
                                </td>
                                <td>
                                    <a href="{!! url('backend/creators/' . $creator->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $creators->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection