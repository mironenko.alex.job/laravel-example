@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование скидки</h1>
            <p class="description">Редактирование скидки</p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/discounts') }}">Скидки</a>
                </li>
                <li class="active">
                    <strong>Редактирование скидки</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/discounts/' . $discount->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $discount->id) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="member-form-inputs">
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('value', 'Cкидка *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('value', $discount->value, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => '0%'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('description', 'Описание', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{  Form::textarea('description', $discount->description, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('class', 'HTML class', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            {{  Form::text('class', $discount->class, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'sell__percent--lightblue'])}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{ url('backend/dicounts') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            <div class="pull-right">
                <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                </button>
            </div>
        </div>

    </div>
    </div>
    {{ Form::close() }}

    <div class="hidden">
        {!! Form::open(['route' => ['discounts.destroy', $discount->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
