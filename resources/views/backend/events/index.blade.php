@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Ивенты</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ url('backend/events/create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить ивент</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="15%">Дата</th>
                            <th width="15%">Город</th>
                            <th width="15%">Название</th>
                            <th width="15%">Стоимость</th>
                            <th width="20%">Учителя</th>
                            <th width="15%">Статус</th>
                            <th width="15%"></th>
                        </tr>
                        <tr>
                            {{ Form::open(['url' => 'backend/events', 'method' => 'GET']) }}
                            <th class="hidden-xs hidden-sm"></th>
                            <th></th>
                            <th>
                                <select name="city_id" class="form-control" data-active="{{ request()->city_id }}">
                                    <option value=""
                                            @if(!request()->has('city_id') || !request()->filled('city_id')) selected="selected" @endif>Выберите
                                    </option>
                                    @forelse($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->title }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </th>
                            <th>{{ Form::text('title', request()->title, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Название"]) }}</th>
                            <th>{{ Form::text('cost', request()->cost, ['class' => 'form-control', 'autocomplete' => false, 'placeholder' => "Стоимость"]) }}</th>
                            <th>
                                <select name="teacher" class="form-control" data-active="{{ request()->teacher }}">
                                    <option value=""
                                            @if(!request()->has('teacher') || !request()->filled('teacher')) selected="selected" @endif>Выберите
                                    </option>
                                    @forelse($teachers as $teacher)
                                        <option value="{{ $teacher->id }}">{{ $teacher->first_name . ' ' . $teacher->last_name }}</option>
                                    @empty
                                    @endforelse
                                </select>
                            </th>
                            <th>
                                <select name="active" class="form-control">
                                    <option value=""
                                            @if(!request()->has('active') || !request()->filled('active')) selected="selected" @endif>Выберите
                                    </option>
                                    <option value="1"
                                            @if(request()->active == 1) selected="selected" @endif>Активен
                                    </option>
                                    <option value="0"
                                            @if(request()->filled('active') && request()->active == 0) selected="selected" @endif>Заблокирован
                                    </option>
                                </select>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i class="fa fa-search">

                                    </i>
                                </button>
                            </th>
                            {{ Form::close() }}
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ url('backend/courses/sort/') }}">
                        @forelse($events as $event)
                            <tr class="js-sortable-item" data-id="{{ $event->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $event->id !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $event->begin !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ isset($event->city) ? $event->city->title : '' }}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email"><a href="{{ route('event', ['event_alias' => $event->alias]) }}" target="_blank">{!! $event->title !!}</a></span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $event->cost !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{{ $event->events_teachers->pluck('first_name')->implode(', ') }}</span>
                                </td>
                                <td class="user">
                                    @if($event->active == 1)
                                        <span class="badge badge-success badge-roundless upper">Активен</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Заблокирован</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! url('backend/events/' . $event->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $events->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection