@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Добавление аудитории хаба</h1>
            <p class="description">Добавление аудитории хаба</p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('hub_rooms') }}">Аудитории хаба</a>
                </li>
                <li class="active">
                    <strong>Добавление аудитории хаба </strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => route('admin_hub_room_create'), 'method' => 'post', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
{{--    {{ Form::hidden('id', $room->id) }}--}}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="member-form-inputs">

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('room_key', 'Ключ *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('room_key', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'укажите ключ ( латиница)'])}}
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('room_name', 'Название *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('room_name', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Введите название'])}}
                            </div>
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('room_image', 'Фото аудитории', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="user-img">
                                <a href="#">
                                    <img data-img="room_image"  src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(null), 100, 100) }}"
                                         class="img-circle js_image_link"
                                         alt="user-pic"/>
                                </a>
                                <div class="invisible">
                                    {{Form::file('room_image', ['accept' => 'image/*', 'data-img' => 'room_image'])}}
                                </div>
                            </div>
{{--                            {{ Form::hidden('room_image_old', $room->room_image) }}--}}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('room_image_location', 'Фото аудитории на плане', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="user-img">
                                <a href="#">
                                    <img data-img="room_image_location" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(null), 100, 100) }}"
                                         class="img-circle js_image_link"
                                         alt="user-pic"/>
                                </a>
                                <div class="invisible">
                                    {{Form::file('room_image_location', ['accept' => 'image/*', 'data-img' => 'room_image_location'])}}
                                </div>
                            </div>
{{--                            {{ Form::hidden('room_image_location_old', }}--}}
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('capacity_text', 'Вместимость *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('capacity_text', null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => ' '])}}
                            </div>
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('room_information', 'Полное описание', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{  Form::textarea('room_information', null, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{route('hub_rooms')}}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            <div class="pull-right">
            </div>
        </div>

    </div>

    {{ Form::close() }}

    {{--<div class="hidden">--}}
{{--        {!! Form::open(['route' => ['discounts.destroy', $room->id], 'method' => 'delete']) !!}--}}
{{--        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}--}}
{{--        {!! Form::close() !!}--}}
    {{--</div>--}}

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
