@extends('backend.layouts.app')
@section('pagetitle')
<div class="page-title">
    <div class="title-env">
        <h1 class="title">Заявки хаба</h1>
    </div>
    <div class="breadcrumb-env">
        <ol class="breadcrumb bc-1">
            <li>
                <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
            </li>
            <li>
                <a href="{{ route('hub_message') }}">Все сообщения</a>
            </li>
            <li class="active">
                <strong>Заявки хаба: {{ \Carbon\Carbon::parse($message->created_at)->format('d-m-Y H:i:s') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection
@section('content')
<div class="panel panel-headerless">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">Основная информация</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main">

                <div class="member-form-inputs">
                    <div class="row form-group">
                        <div class="col-sm-3">Имя пользователя</div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{ $message->name }}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">Email</div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{ $message->email }}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">Телефон</div>
                        <div class="col-sm-9">
                            {!! $message->tel !!}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">Текст заявки</div>
                        <div class="col-sm-9">
                            {!! $message->text !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{route('hub_message')}}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
        </div>
    </div>
</div>

@endsection

