@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование страницы</h1>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['route' => 'admin_hub_update', 'method' => 'post', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true, 'multiple'=>'multiple']) }}
    {{ Form::hidden('id', isset($data->id) ? $data->id : 1) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
                <li>
                    <a href="#seo" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">SEO</span>
                    </a>
                </li>
                <li>
                    <a href="#contact" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Контакты</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">

                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <!-- главный текст №1 -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_introduction_title', 'Название *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('section_introduction_title', isset($data) && $data->section_introduction_title ? $data->section_introduction_title : '', ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>
                        <!-- главный текст №2 -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_introduction_description', 'Описание', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('section_introduction_description', isset($data) && $data->section_introduction_description ? $data->section_introduction_description : '', ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_class_rooms_title', 'Блок аудиторий, название', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('section_class_rooms_title', isset($data) && $data->section_class_rooms_title ? $data->section_class_rooms_title : '', ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_class_rooms_description', 'Блок аудиторий, описание', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('section_class_rooms_description', isset($data) && $data->section_class_rooms_description ? $data->section_class_rooms_description : '', ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_get_info_title', 'Текст перед галереей', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('section_get_info_title', isset($data) && $data->section_get_info_title ? $data->section_get_info_title : '', ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('section_about_us_content', 'Блок "О нас" (контент)', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    {{  Form::textarea('section_about_us_content', isset($data) && $data->section_about_us_content ? $data->section_about_us_content : '', ['class' => 'form-control ckeditor', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                                </div>
                            </div>
                        </div>


                        <!-- О нас -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('about_section_thumbnail', 'Добавить новое фото в галерею', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="user-img">
                                    <a href="#">
                                        <img data-img="about_section_thumbnail"
                                             src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('homepage')->url(  isset($data) && $data->about_section_thumbnail ? $data->about_section_thumbnail : ''), 100, 100) }}"
                                             class="img-circle js_image_link"
                                             alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('about_section_thumbnail', ['accept' => 'image/*', 'data-img' => 'about_section_thumbnail'])}}
                                    </div>
                                </div>
                                {{ Form::hidden('about_section_thumbnail_old', isset($data) && $data->about_section_thumbnail ? $data->about_section_thumbnail : '') }}
                            </div>
                        </div>

                    {{--<div class="row form-group">--}}
                    {{--<div class="col-sm-3">--}}
                    {{--{{ Form::label('about_section_content', 'Блок "О нас" (контент)', ['class' => 'control-label']) }}--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-9">--}}
                    {{--<div class="input-group">--}}
                    {{--{{  Form::textarea('about_section_content', $data->about_section_content, ['class' => 'form-control ckeditor', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    <!--Фото галереи -->
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('opinion_section_thumbnail', 'Фото галереи', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="user-img .center-block">
                                    @if($images)
                                        @foreach($images as $img)
                                            <img src="{{$img->image}}" data-id={{$img->id}} width="150" height="150" class="float-left">
                                        @endforeach
                                    @endif
                                    <div class="invisible">
                                        {{Form::file('opinion_section_thumbnail', ['accept' => 'image/*', 'data-img' => 'opinion_section_thumbnail'])}}
                                    </div>
                                </div>
                                {{ Form::hidden('opinion_section_thumbnail_old', isset($data) && $data->opinion_section_thumbnail ? $data->opinion_section_thumbnail : '') }}
                            </div>
                        </div>


                    </div>
                </div>
                <div class="tab-pane" id="seo">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-title</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_title', isset($data) && $data->seo_title ? $data->seo_title : '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">SEO-keywords</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_keywords', isset($data) && $data->seo_keywords ? $data->seo_keywords : '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-robots</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_robots', isset($data) && $data->seo_robots ? $data->seo_robots : '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-canonical</label>
                        <div class="col-sm-9">
                            {{  Form::text('seo_canonical', isset($data) && $data->seo_canonical ? $data->seo_canonical : '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3 control-label" for="field-1">SEO-description</label>
                        <div class="col-sm-9">
                            {{  Form::textarea('seo_description', isset($data) && $data->seo_description ? $data->seo_description : '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="contact">
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Телефон</label>
                        <div class="col-sm-9">
                            {{  Form::text('phone', isset($data) && $data->phone ? $data->phone : '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1">Email</label>
                        <div class="col-sm-9">
                            {{  Form::text('mail', isset($data) && $data->mail ? $data->mail : '', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-3" for="field-1" for="field-1">Адрес</label>
                        <div class="col-sm-9">
                            {{  Form::text('address', isset($data) && $data->address ? $data->address : '', ['class' => 'form-control']) }}
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@include('backend.modals.image')

@include('backend.modals.image-remove')

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
