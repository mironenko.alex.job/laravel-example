@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">О нас говорят</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ route('opinions.create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="10%">Лого</th>
                            <th width="25%">Название</th>
                            <th width="25%">Ссылка</th>
                            <th width="15%"></th>
                        </tr>
                        </thead>
                        <tbody class="js-sortable" data-url="{{ route('opinions.sort') }}">
                        @forelse($data as $partner)
                            <tr class="js-sortable-item" data-id="{{ $partner->id }}">
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $partner->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('opinions')->url(  $partner->id . '/' . $partner->logo  ), 239, 239) }}" class="img-circle" alt="user-pic"/>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $partner->title !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <a href="{!! $partner->link !!}" class="email" target="_blank">{!! $partner->link !!}</a>
                                </td>
                                <td>
                                    <a href="{!! url('backend/opinions/' . $partner->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection