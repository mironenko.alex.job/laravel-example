@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование мероприятия  </h1>
            <p class="description">Редактирование программы </p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('project_programms') }}">Программа</a>
                </li>
                <li class="active">
                    <strong>Редактирование мероприятия</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['route' => ['project_programm_update',$data->id], 'method' => 'post', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $data->id) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="member-form-inputs">
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('title', 'Название мероприятия *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('title',$data->title,['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('workshop', 'Workshop', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                <select name="workshop" class="form-control">
                                    @if($data->workshop == 1)
                                        <option selected value="1">Да</option>
                                        <option value=" ">Нет</option>
                                    @else
                                    <option value=" ">Нет</option>
                                    <option value="1">Да</option>
                                        @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    @if($data->workshop == 1)
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('seats',  'Вместимость  *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('seats',$data->seats,['class' => 'form-control','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Укажите число'])}}
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('time', ' Время *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                <select name="time" class="form-control">
                                    <option value="{{ $data->time }}">{{ $data->time }}</option>
                                    @foreach ($times as $time)
                                        <option value="{{ $time->time }}">{{ $time->time }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('room', ' Комната *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                <select name="room" class="form-control">
                                    <option selected value="{{$data->room}}">{{$data->room}}</option>
                                    <option value="all">Все</option>
                                    <option value="gray">Grey room</option>
                                    <option value="green">Green room</option>
                                    <option value="blue">Blue room</option>
                                    <option value="red">Red room</option>
                                    <option value="class">Class room</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('short_text', '  краткий текст  *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('short_text',$data->short_text,['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => ' '])}}
                            </div>
                        </div>
                    </div>



                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('text', 'Полный текст', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{  Form::textarea('text',$data->text,['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                    </div>

                    @if(count($speakers) > 0)
                        <div class="row form-group">
                            <label class="col-sm-3 control-label" for="organization_id">Учителя</label>
                            <div class="col-sm-9">
                                <select name="teacher_id[]" id="teacher_id" multiple class="form-control js_ch_multiple">
                                    @foreach($speakers as $speaker)
                                        <option value="{{ $speaker->id }}" @if(in_array($speaker->id, $project_speakers)) selected="selected" @endif>{{ $speaker->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{route('project_programms')}}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            <div class="pull-right">
                <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                </button>
            </div>
        </div>

    </div>

    {{ Form::close() }}
    <div class="hidden">
        {!! Form::open(['route' => ['project_programms_del', $data->id], 'method' => 'post']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
