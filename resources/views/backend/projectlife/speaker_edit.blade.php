@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование спикера </h1>
            <p class="description">Редактирование спикера </p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ route('project_speakers') }}">Спикеры</a>
                </li>
                <li class="active">
                    <strong>Редактирование спикера  </strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['route' => ['project_speaker_update', $speaker->id], 'method' => 'post', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $speaker->id) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">

                <div class="member-form-inputs">
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('тфьу', 'Имя\Фамилия *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('name', $speaker->name, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Название'])}}
                            </div>
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('img', 'Фото спикера', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="user-img">
                                <a href="#">
                                    <img data-img="img" src="{{$speaker->img}}"
                                         class="img-circle js_image_link"
                                         alt="user-pic"/>
                                </a>
                                <div class="invisible">
                                    {{Form::file('img', ['accept' => 'image/*', 'data-img' => 'img'])}}
                                </div>
                            </div>
                            {{ Form::hidden('img_old', $speaker->img) }}
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('img_hover', 'Фото по ховеру', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="user-img">
                                <a href="#">
                                    <img data-img="img_hover" src="{{$speaker->img_hover}}"
                                         class="img-circle js_image_link"
                                         alt="user-pic"/>
                                </a>
                                <div class="invisible">
                                    {{Form::file('img_hover', ['accept' => 'image/*', 'data-img' => 'img_hover'])}}
                                </div>
                            </div>
                            {{ Form::hidden('img_hover_old', $speaker->img_hover) }}
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('short_text', 'Краткий текст *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-pencil"></i></a>
                                </div>
                                {{  Form::text('short_text', $speaker->short_text, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => ' '])}}
                            </div>
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('text', 'Полный текст', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {{  Form::textarea('text', $speaker->text, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{route('project_speakers')}}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            <div class="pull-right">
                <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                </button>
            </div>
        </div>

    </div>

    {{ Form::close() }}

    <div class="hidden">
        {!! Form::open(['route' => ['project_speaker_dell', $speaker->id], 'method' => 'post']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('!!!! сделать удалялку !!! Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
