@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Спикеры </h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{route('project_speaker_add')}}" class="btn btn-success"><span class="fa-plus"></span> Добавить спикера</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="20%">Имя\Фамилия </th>
                            <th width="10%">Фото</th>
                            <th width="20%">Фото по ховеру</th>
                            <th width="20%">Краткий текст</th>
                            <th width="20%">Полный текст</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($speakers as $speaker)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $speaker->name !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{$speaker->img}}" class="img-responsive" alt="{{$speaker->img}}" />
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <img src="{{$speaker->img_hover}}" class="img-responsive" alt="{{$speaker->img_hover}}" />
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $speaker->short_text }}</span>

                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $speaker->text }}</span>
                                </td>


                                <td>
                                    <a href="{{route('speaker_edit',['id' => $speaker->id])}}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
