@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Промокоды</h1>
        </div>
        <div class="breadcrumb-env">
            <a href="{{ url('backend/promocodes/create') }}" class="btn btn-success"><span class="fa-plus"></span> Добавить промокод</a>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">id</th>
                            <th width="25%">Действителен до</th>
                            <th width="10%">Скидка</th>
                            <th width="15%">В процентах</th>
                            <th width="15%">Тип</th>
                            <th width="10%">Статус</th>
                            <th width="15%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($promocodes as $promocode)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $promocode->id !!}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $promocode->valid_until)->format('d.m.Y') }}</span>
                                </td>
                                <td class="user-image hidden-xs hidden-sm">
                                    <span class="email">{{ $promocode->discount }}</span>
                                </td>
                                <td class="user">
                                    @if($promocode->in_percent == 1)
                                        <span class="badge badge-success badge-roundless upper">да</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">нет</span>
                                    @endif
                                </td>
                                <td class="user">
                                    @if($promocode->reusable == 1)
                                        <span class="badge badge-success badge-roundless upper">Многоразовый</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Одноразовый</span>
                                    @endif
                                </td>
                                <td class="user">
                                    @if($promocode->active == 1)
                                        <span class="badge badge-success badge-roundless upper">Активен</span>
                                    @else
                                        <span class="badge badge-red badge-roundless upper">Заблокирован</span>
                                    @endif
                                </td>

                                <td>
                                    <a href="{!! url('backend/promocodes/' . $promocode->id . '/edit') !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $promocodes->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection