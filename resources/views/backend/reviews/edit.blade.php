@extends('backend.layouts.app')
@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
            <h1 class="title">Редактирование отзыва</h1>
            <p class="description">Редактирование отзыва</p>
        </div>
        <div class="breadcrumb-env">
            <ol class="breadcrumb bc-1">
                <li>
                    <a href="{{ url('backend/dashboard') }}"><i class="fa-home"></i>Главная</a>
                </li>
                <li>
                    <a href="{{ url('backend/reviews') }}">Отзывы</a>
                </li>
                <li class="active">
                    <strong>Редактирование отзыва</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection
@section('content')
    {{ Form::open(['url' => 'backend/reviews/' . $review->id, 'method' => 'PUT', 'class' => 'validate form-horizontal js_form', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product', 'files' => true]) }}
    {{ Form::hidden('id', $review->id) }}
    {{ Form::hidden('old_thumbnail', $review->thumbnail) }}
    <div class="panel panel-headerless">
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#main" data-toggle="tab">
                        <span class="visible-xs"><i class="fa-user"></i></span>
                        <span class="hidden-xs">Основная информация</span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="main">
                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <div class="user-img">
                                    <a href="#" class="js_form-link">
                                        <img id="photo_img" src="{{ \App\Helpers\Helpers::getImageCache(Storage::disk('reviews')->url(  $review->id . '/' . $review->thumbnail  ), 100, 100) }}"
                                             class="img-circle"
                                             alt="user-pic"/>
                                    </a>
                                    <div class="invisible">
                                        {{Form::file('thumbnail', ['accept' => 'image/*', 'id' => 'photo'])}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('username', 'Автор *', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-child"></i></a>
                                    </div>
                                    {{  Form::text('username', $review->username, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Автор'])}}
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="fa-link"></i></a>
                                    </div>
                                    {{  Form::text('email', $review->email, ['class' => 'form-control', 'data-message-required' => 'Поле обязательно для заполнения', "placeholder" => 'Email'])}}
                                </div>
                            </div>
                        </div>
                        @if(count($courses) > 0)
                            <div class="row form-group">
                                <label class="col-sm-3" for="organization_id">Курс</label>
                                <div class="col-sm-9">
                                    <select name="course_id" id="course_id" class="form-control" data-active="{{ $review->course_id }}">
                                        <option>Выберите</option>
                                        @foreach($courses as $course)
                                            <option value="{{ $course->id }}">{{ $course->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <label class="col-sm-3" for="field-1">Текст отзыва</label>
                            <div class="col-sm-9">
                                {{  Form::textarea('review', $review->review, ['class' => 'form-control ckeditor']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('active', 'Активность', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('active', 1, $review->active, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-3">
                                {{ Form::label('from_student', 'От студента', ['class' => 'control-label']) }}
                            </div>
                            <div class="col-sm-9">
                                {{ Form::checkbox('from_student', 1, $review->from_student, ['class' => 'iswitch iswitch-secondary']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group">
                <a href="{{ url('backend/reviews') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
                <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
                </button>
                <div class="pull-right">
                    <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="hidden">
        {!! Form::open(['route' => ['reviews.destroy', $review->id], 'method' => 'delete']) !!}
        {!! Form::submit('Удалить', ['class' => 'btn btn-danger js_destroy', 'onclick' => "return confirm('Вы уверены?')"]) !!}
        {!! Form::close() !!}
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/select2/select2-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('assets/js/multiselect/css/multi-select.css') }}">
@endsection

@section('scripts')
    <script src="{{ url('assets/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('assets/js/select2/select2.min.js') }}"></script>
    <script src="{{ url('assets/js/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ url('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ url('assets/js/inputmask/jquery.inputmask.bundle.js') }}"></script>
@endsection
