<div class="panel panel-headerless">
    <div class="panel-body">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#main" data-toggle="tab">
                    <span class="visible-xs"><i class="fa-user"></i></span>
                    <span class="hidden-xs">Основная информация</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main">
                <div class="member-form-inputs js_parent_div" data-csrf="{{ csrf_token() }}">
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('typeIds', 'Тип *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {!! Form::select('typeIds[]', $types, (isset($test) ? $test->tests_ttypes()->pluck('id')->toArray() : null), ['class'=>'form-control js_ch_multiple', 'multiple' => 'multiple']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('key', 'Ключ *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::text('key', isset($test) ? $test->key : null, ['class' => 'form-control', 'maxlength' => '4','data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', 'required' => 'required', "placeholder" => 'Ключ'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('question', 'Вопрос *', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::textarea('question', isset($test) ? $test->question : null, ['class' => 'form-control', 'data-validate' => 'required', 'data-message-required' => 'Поле обязательно для заполнения', 'required' => 'required', "placeholder" => 'Вопрос'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-3">
                            {{ Form::label('position', 'Позиция', ['class' => 'control-label']) }}
                        </div>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <a href="#"><i class="fa-child"></i></a>
                                </div>
                                {{  Form::number('position', isset($test) ? $test->position : null, ['class' => 'form-control', "placeholder" => 'Позиция'])}}
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="row form-group">
                            <label class="col-sm-3" for="js_clients_phones">Ответы</label>
                            <div class="col-sm-9 js_parent">
                                @if(isset($test) && $test->answers)
                                    @foreach ($test->answers as $answer)
                                        <div class="input-group js_section">
                                            <div class="input-group-addon">
                                                <a href="#"><i class="fa-link"></i></a>
                                            </div>
                                            {{  Form::text('answers[key][]', $answer->key, ['class' => 'form-control', 'maxlength' => '3', 'data-validate' => 'required', "placeholder" => 'Ключ'])}}
                                            {{  Form::text('answers[answer][]', $answer->answer, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Ответ'])}}
                                            {{  Form::hidden('answers[id][]', $answer->id)}}
                                            <div class="input-group-addon js_remove_item">
                                                <a href="#">-</a>
                                            </div>
                                            <div class="input-group-addon js_add_item">
                                                <a href="#">+</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="input-group js_section">
                                        <div class="input-group-addon">
                                            <a href="#"><i class="fa-link"></i></a>
                                        </div>
                                        {{  Form::text('answers[key][]', null, ['class' => 'form-control',  'maxlength' => '3', 'data-validate' => 'required', "placeholder" => 'Ключ'])}}
                                        {{  Form::text('answers[answer][]', null, ['class' => 'form-control', 'data-validate' => 'required', "placeholder" => 'Ответ'])}}
                                        <div class="input-group-addon js_remove_item">
                                            <a href="#">-</a>
                                        </div>
                                        <div class="input-group-addon js_add_item">
                                            <a href="#">+</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{ url('backend/types') }}" class="btn btn-white"><span class="fa-arrow-left"></span> Назад</a>
            <button type="submit" class="btn btn-success js_form_submit"><span class="fa-save"></span> <b>Сохранить</b>
            </button>
            @isset($test)
                <div class="pull-right">
                    <button class="btn btn-danger js_remove"><span class="fa-remove"></span> <b>Удалить</b>
                    </button>
                </div>
            @endisset
        </div>
    </div>
</div>