@extends('backend.layouts.app')

@section('pagetitle')
    <div class="page-title">
        <div class="title-env">
                <h1 class="title">Типы тестов</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="tab-content">
                <div class="tab-pane active" id="all">

                    <table class="table table-hover members-table middle-align">
                        <thead>
                        <tr>
                            <th width="10%">Ключ</th>
                            <th width="20%">Название</th>
                            <th width="60%">Курсы</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($ttypes as $ttype)
                            <tr>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $ttype->id !!}</span>
                                </td>

                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $ttype->name !!}</span>
                                </td>
                                <td class="hidden-xs hidden-sm">
                                    <span class="email">{!! $ttype->courses_ttypes->pluck('title')->implode(', ') !!}</span>
                                </td>
                                <td>
                                    <a href="{!! route('ttypes.edit', $ttype->id) !!}" class="btn btn-sm btn-success">
                                        <i class="linecons-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6"><p class="text-center">По данному запросу результатов не найдено.</p></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <div class="row">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right text-center-sm">
                            {{ $ttypes->appends([request()])->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
