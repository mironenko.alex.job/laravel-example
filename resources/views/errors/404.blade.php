@extends('frontend.layouts.404')

@push('styles')
<link rel="stylesheet" href="/css/404.css">
@endpush

@section('content')
    <body class="body body-404">
        <div class="block">
            <div class="block__left">
                <img src="/img/404.png" alt="404" class="block__img">
            </div>
            <div class="block__right">
                <p class="block__title">Упс!</p>
                <p class="block__text">Походу я потерялся, верните меня на сайт</p>
                <div>
                    <a href="{{ route('home') }}" class="btn btn--orange btn--bigger">На главную</a>
                </div>
            </div>
            
        </div>
    </body>
@endsection
