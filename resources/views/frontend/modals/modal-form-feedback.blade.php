<div class="modal noscroll" data-noscroll="true" id="modalFeedback">
    <div class="modal-feedback">
        <div class="modal-feedback__wrapper">
            <form class="checkin js-form" action="{{ route('add-feedback') }}" method="post" id="feedbackForm">
                <div class="checkin__heading">Записаться на курс</div>
                <div class="checkin-reg">Я,
                    <input class="checkin__input checkin__input--offset" type="text" placeholder="Фамилия и Имя"
                           name="username">, хочу записаться в<span class="no-br pd-both">A-Level</span>Ukraine на курс
                    <div class="cities-id blue-select fix-width noscroll" data-noscroll="true">
                        <select class="checkin__select" id="choose-course1" name="course_id">
                            @forelse($courses as $course)
                                <option value="{{ $course->id }}"
                                        data-noscroll="true"
                                @if($routeName == 'course' && $course->id == $courseData->id) selected="selected" @endif>{!! $course->title !!}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <!--Мой город -
                    <div class="cities-id blue-select noscroll">
                        <select class="checkin__select" id="choose-city1" name="city_id">
                            @forelse($cities as $city)
                                <option value="{{ $city->id }}"
                                        @if($city->id == session('city')) selected="selected" @endif>{!! $city->title !!}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>-->
                    Свяжитесь со мной по телефону
                    <input class="checkin__input checkin__phone js-mask" name="phone" type="text"
                           placeholder="+38(050) 000-00-00">
                    <br>
                    или по e-mail
                    <input class="checkin__input checkin__email" name="email" type="text" placeholder="Ваш email">
                </div>
                <div class="checkin__text js-error-area" style="display: none"></div>
                <div class="form__btn">
                    <button class="btn btn--orange btn--bigger" type="submit">Отправить заявку</button>
                </div>
                <div class="checkin__text">*Вы принимаете политику конфиденциальности. Ваш адрес электронной почты и ФИО
                    ни в коем случае не будут переданы другим пользователям и никогда не будут открыты для просмотра.
                    Мы не будем передавать эти данные третьим лицам и/или любым другим способом сообщать эти данные
                    третьим лицам.
                </div>
            </form>
        </div>
        <div class="modal-close"><i class="icon-close"></i></div>
    </div>
</div>