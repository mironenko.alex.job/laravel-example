@extends('frontend.layouts.test')

@isset($data)
    @section('title', $data->seo_title)

    @push('seo_fields')
        <meta name="description"
              content="{{ $data->seo_description }}">
        <meta name="keywords" content="{{ $data->seo_keywords }}">
        <meta name="robots" content="{{ $data->seo_robots }}">
        @if($data->seo_canonical)
            <link rel="canonical" href="{{ $data->seo_canonical  }}"/>
        @endif
    @endpush

@endisset

@push('styles')
    <link rel="stylesheet" href="/css/testing.css">
@endpush

@section('content')

    <body class="body testing-page">
    <section class="section course-main">
        <div class="double-bg">
            <div class="double-bg__left"></div>
            <div class="double-bg__right"></div>
        </div>
        <div class="window">
            <div class="window-left"><img class="window-left__img" src="/img/testing/choose.png" alt="army"></div>
            <div class="window-right">
                <p class="testing__heading" id="choose__p">Тестирование на выбор курса</p>
                <div class="animator" id="questions">
                    <div class="testing-start" data-slide-testing="1">
                        <p class="testing__p" id="testing__p">
                            @isset($data)
                                {{ $data->description }}
                            @endisset
                        </p>
                    </div>

                </div>
                <div class="testing__btns">
                    <a href="{{ route('tests.question', ['id' => $test->id]) }}" class="btn btn--orange" data-direction="1">Начать тестирование</a>
                    <a href="{{ route('home') }}" class="btn btn--blue">Перейти на выбор курса</a>
                </div>
            </div>
        </div>
    </section>
    @include('frontend.tests.template-parts')
    <!-- Loading JS Start-->
    @push('scripts')
        <script defer src="/js/commons.js"></script>
        <script defer src="/js/testing.js"></script>
    @endpush
    <!-- Loading JS End-->
    </body>

@endsection


